<?php
/*
A cookie associated with a cross-site resource at http://fcmserver2.bo.infn.it/ was set without the `SameSite` attribute. A future release of Chrome will only deliver cookies with cross-site requests if they are set with `SameSite=None` and `Secure`. You can review cookies in developer tools under Application>Storage>Cookies and see more details at https://www.chromestatus.com/feature/5088147346030592 and https://www.chromestatus.com/feature/5633521622188032.
allfunctions.js:229 yyyy
*/
require 'allglobal.php';
function Num2Pos($N,$SUBPART)
{
	$con=OpenReadConn();
	$sql="SELECT POS FROM DOMPOSPBS WHERE POS LIKE '%(".$N.")%'";
	$res=$con->query($sql)->fetch_assoc()['POS'];
	$con->close();
	return $res;
}
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) 
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    return $randomString;
}
function readallcookies()
{
	$cipher = "AES-128-CBC";
	$ivlen = openssl_cipher_iv_length($cipher);
    $iv = openssl_random_pseudo_bytes($ivlen);
	if (isset($_SERVER['HTTP_COOKIE'])) 
		{   $iv=hex2bin($_SESSION['iv']);
			$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			foreach($cookies as $cookie) 
			{   
				$parts = explode('=', $cookie);
				$name = trim($parts[0]);
				$_COOKIE[$name]=trim($parts[1]);
				if(ENCRYPTCOOKIE && !(strpos($name, 'CU_') === 0))
				$_COOKIE[$name] = openssl_decrypt(hex2bin(trim($parts[1])), $cipher ,$_SESSION['key'], $options=OPENSSL_RAW_DATA,$iv);
	
			}
			
			
		}
}
readallcookies();
function setencryptedcookie($name,$value)
{   $cipher = "AES-128-CBC";
	$iv=hex2bin($_SESSION['iv']);
	if(ENCRYPTCOOKIE)
	$value=bin2hex(openssl_encrypt ( $value , $cipher ,$_SESSION['key'],$options=OPENSSL_RAW_DATA, $iv));
	setcookie($name, $value);
}
function CreateTestSheet($T_id,$UPI)
{
			//print_r("CALL TO CTSheet".$T_id,$UPI);
			//debug_print();
			$conn=OpenWriteConn();
			$sql="INSERT into Test (`T_id`,`UPI`) VALUES ('".$T_id."','".$UPI."')";
			if  (!($conn->query($sql))) sqlog("ERROR IN TEST MENU :".$sql,$conn->error) ;
			$sql="SELECT LAST_INSERT_ID();"; //SCOPE_IDENTITY() for modules 
			$last=$conn->query($sql);
			$Last_test=$last->fetch_assoc()['LAST_INSERT_ID()'];// Get the generated test_id
			
			$sql="SELECT * FROM Test_Measures  inner join Measure_Type on Test_Measures.M_T_id=Measure_Type.M_T_id   where Test_Measures.T_id=".$T_id."  ;";
			$maesures= $conn->query($sql);
			if (!$maesures) sqlog("ERROR IN TEST MENU :".$sql,$con->error);

			while($mae = $maesures->fetch_assoc()) {
			$sql="INSERT into Measure (`M_id`,`E_T_id`) VALUES ('".$mae['M_id']."','".$Last_test."')";
			if  (!($conn->query($sql))) sqlog($sql,$conn->error) ; // Inserts a new empty record for each mesure 
					}
			$conn->close();
			return $Last_test;
}
function CheckTest($UPI)
{
	$con= OpenReadConn();
	$sql="Select * From ASSIGNED_DOM as ASD JOIN IntegrationPhase as IPH ON ASD.PhaseID=IPH.PhaseID 
				 where ASD.UPI='".$UPI."';";
	$result = $con->query($sql);
	if($result) $DestinationRow = $result->fetch_assoc() ; else
	{
				sqlog($sql,$con->error);
				return false;
	}			
    $Mval=array();
	$Last_test=array();
	$ave_mval=0;
	$sql="SELECT * FROM Test_Type where PhaseId='".$DestinationRow['PhaseID']."';";
	$Tests = $con->query($sql); // The test done in this integration phase
	if($Tests->num_rows==0) return true; //No test to be done so everything is complete
	$NTest=$Tests->num_rows;
	
	while($row = $Tests->fetch_assoc()) {
			
			$sql="SELECT * FROM Test t1 where T_Id='".$row['T_id']."' AND UPI='".$UPI."'
		     AND T_Time=(SELECT MAX(T_Time) FROM Test t2 where t2.UPI='".$UPI."');";
		$Done_Tests=$con->query($sql); //Search last_test of that type done for the current DOMUPI
		if (!$Done_Tests) {sqlog($sql,$con->error);}
			else
			{
				if ($Done_Tests->num_rows>0)
				{
					$Last_test[$row['T_id']]=$Done_Tests->fetch_assoc()['E_T_id'];
					
					
				}
				else
			{// Previous result NOT found
				return false; //WHAT IF THE PART HAS NO TEST TO DO ?
			}
			}
$sql="SELECT ((SELECT COUNT(*) FROM Measure Me inner join Test_Measures on Me.M_id=Test_Measures.M_id inner join Measure_Type on Test_Measures.M_T_id=Measure_Type.M_T_id   
      where Test_Measures.T_id=".$row['T_id']." AND Me.E_T_id=".$Last_test[$row['T_id']]." )-
(SELECT COUNT(*) FROM Measure Me inner join Test_Measures on Me.M_id=Test_Measures.M_id inner join Measure_Type on Test_Measures.M_T_id=Measure_Type.M_T_id   
      where Test_Measures.T_id=".$row['T_id']." AND Me.E_T_id=".$Last_test[$row['T_id']]." AND Me.VALUE IS NOT NULL AND Me.VALUE<>'' ) ) as empty_pos,
	 (SELECT COUNT(*) FROM Measure Me inner join Test_Measures on Me.M_id=Test_Measures.M_id inner join Measure_Type on Test_Measures.M_T_id=Measure_Type.M_T_id   
      where Test_Measures.T_id=".$row['T_id']." AND Me.E_T_id=".$Last_test[$row['T_id']]." ) as all_pos ;";
	$result= $con->query($sql);
	if (!$result)
	{ sqlog($sql,$con->error); }
	else 
	{
	$measure_count=$result->fetch_assoc();
	$Mval[$row['T_id']]=100*($measure_count['all_pos']-$measure_count['empty_pos'])/$measure_count['all_pos'];
	$ave_mval+=$Mval[$row['T_id']];
	
	if ($Mval[$row['T_id']]<100) return false;
	}
	}// END LOOP ON TESTS
	return true;
}

function cleanscan($scan)
{
		if (strstr($scan, 'http')<>"") $scan=strstr($scan, 'http');
		$scan=str_replace("https://km3netdbweb.in2p.fr/product/","",$scan);
		$scan=str_replace("https://km3netdbweb.in2p3.fr/product/","",$scan);
		$scan=str_replace("http://km3netdbweb.in2p3.fr/product/","",$scan);
		$scan=rtrim($scan,'.');
		$scan=trim($scan);
		//print_r("CODE: ".$scan."  ");
		return $scan;	
}
function debug_print()
{
	// Debug printout
	if (DEBUG) // Defined in alglobal
	{
		$caller=debug_backtrace();
		var_dump($caller); // Note that also the arguments will be printed by this call.
	/*	$arg_list = func_get_args();
		foreach ($arg_list as $arg)
		    var_dump($arg); */
		//sleep(3);
	}
	
}
function hasclb($UPI)
{
	/*
	Check if a subpart has the CLB. Returns 0 or 1.
	*/
	$pbs=UPI2TOK($UPI)['PBS'];
	$con=OpenReadConn();
	$sql="select EXISTS(select 1 from DOMPOSPBS where SUBPART='".$pbs."' and PBS='3.4.3.2');";
	$result = $con->query($sql);
	$con->close();
	$row = $result->fetch_row();
	return $row[0];
	
}
function setlangcookie()
{
	
	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		//print_r("AFterIF");
	$cookie_name = "km3lang";
	if(isset($_REQUEST['lang'])){
		//print_r("AFterIF2");
	$cookie_value = $_REQUEST['lang'];
	setencryptedcookie($cookie_name, $cookie_value);
	} 
}else if(!isset($_COOKIE['lang']))
	{	$cookie_name = "km3lang";
		$cookie_value ='en';
		setencryptedcookie($cookie_name, $cookie_value);
	}
	
	
}
	
function manubar()
	{
	
	echo '<div class="class="w3-dropdown-hover"">';
	if($_COOKIE['km3user']<>"")
	echo ' <a href="logon_ok.php" class="w3-bar-item w3-button"><i class="fas fa-home" style="font-size:24px"></i></a>';
	if($_COOKIE['km3user_MON']<>"")
	echo ' <a href="logon_ok_MONITOR.php" class="w3-bar-item w3-button"><i class="fas fa-home" style="font-size:24px;color:darkgreen"></i></a>';

	//echo ' <a href="#" class="w3-bar-item w3-button"> <i class="fas fa-user"></i></a>';

	if (func_num_args() == 0) {
	echo ' <button onclick="startscanner(\'QRC\')" TITLE="START WEBCAM TO SCAN QR" class="w3-bar-item w3-button w3-right"><i class="fa fa-camera" style="font-size:24px;color:darkblue"></i></button>';
	}
	
	echo ' <button  onclick="on()" id="editlb" class="w3-bar-item w3-button w3-right"><i class="far fa-edit" style="font-size:24px"></i></button>';
	echo ' <button   onclick="window.open(\'readlogbook.php\', \'_blank\');"  id="readlb" class="w3-bar-item w3-button w3-right"><i class="fas fa-book" style="font-size:26px"></i></button>	<SCRIPT> var user= getCookie("km3user");
	if(getCookie("km3user")!=""&&getCookie("Ilocation")!="")
	{
	document.getElementById("editlb").style.display="block";
	document.getElementById("readlb").style.display="block";
	} else
	{
	document.getElementById("editlb").style.display="none";
	document.getElementById("readlb").style.display="none";
	}
	</SCRIPT>';
	echo '</div>';
    LogB_overlay(); // Generate the HTML code for LogBook
	echo '<div class="w3-cell-row" >';
	echo '<div class="w3-container w3-cell w3-light-blue "  style="width:50%">';
	
	}
	
function LogB_overlay()
{   //HTML CODE FOR LOGBOOK OVERLAY
    $Fname= date("dmy_hms").'N'.$_COOKIE['Ilocation'];
	
	echo ' 
	
	<div id="overlay"  style="
    position: fixed;
    display: none;
    width: 100%; height: 100%;
    top: 0; left: 0; right: 0; bottom: 0;
    background-color: rgba(0,0,50,0.5);
    z-index: 2; cursor: pointer; ">  <div style="position: absolute;
  width: 50%;
    top: 50%; left: 10%;
    font-size: 20px;
    color: white; transform: translate(-0%,-50%);
    -ms-transform: translate(-0%,-50%);"> Subject
	<button  onclick="off()" class=" w3-button w3-right"><i class="far fa-window-close"></i></button> <div id="count"></div>  <FORM method="post">  <INPUT TYPE="TEXT"  name="KM3_subj"  maxlength="40" style="width: 100%; padding: 12px 20px;
    margin: 8px 0;" >Comment </INPUT>  <TEXTAREA maxlength="'.MAX_BUFF.'" cols="40" rows="5" name="KM3_comment" id="textarea" style="width: 100%;
    padding: 12px 20px;
    margin: 8px 0;"></textarea>  
  <SCRIPT>  document.getElementById("textarea").onkeyup = function () {
  document.getElementById("count").innerHTML = "Characters left: " + (500 - this.value.length);
};
</SCRIPT>  <input id="Fname" name="Fname" type="hidden" value="'.$Fname.'">   <button  title="Submit Note" TYPE="SUBMIT"  class=" w3-button w3-right" ><i class="far fa-check-square"></i></button>  
    </FORM>  </div> 
<DIV
style="position: absolute;
    top: 50%; left: 50%;
    font-size: 20px;
    color: white; transform: translate(50%,-50%);
    -ms-transform: translate(50%,-50%);">	PREVIEW:
	<div id="my_camera" style="width:310; height:240;"></div>	RESULT:
	<div id="my_result" style="width:310; height:240;"></div>
	
<button  type="button"  title="Add an Image" onclick="take_snapshot(\''.$Fname.'\')" class=" w3-button ">';
  echo "<i class='far fa-image' style='font-size: 24pt;'></i> SNAP</button>
	
</DIV>	</div>";
}

function Hi_left()
{
	echo ' <BR><hr><div class="w3-container  w3-cell "><div id="message"class="w3-container  w3-cell "></div><br>';
	echo '<div class=" w3-container w3-cell w3-cell-middle" id="qrcode" style="position: relative;
    z-index: 99;"></div>';
	echo '<video id="preview" style="width:50%" ></video>';
	}
function ShowMessage()
{ /* 
Show Message in "message" div. If also a Language (normally $ht_string[0]) is provided shows 
utter button.
*/
	if (func_num_args() != 0) {
		$args = func_get_args();
		
		
		if (func_num_args()== 1)
				{
		echo '<script type="text/javascript">document.getElementById("message").innerHTML ="'.$args[0].'"</script>';
				}
			
			if (func_num_args()== 2)
			{
				$pre='<button onclick="responsiveVoice.speak(';
				
				$string=$args[0].$pre."\'".$args[0]."\' , \'".$args[1]."\'";
				$string=$string.')" class="  w3-button w3-right"><i class="fa fa-volume-up"></i></button>';
				
				echo '<script>';
				echo " let string='".$string."';";
				echo 'document.getElementById("message").innerHTML =string.toString();</script>';

			}
	}
}

	
	function Low_left()
{
	echo ' </div><Br></div><hr><div class="w3-container  w3-cell w3-cell-middle">';
	
}	
function footbar() {
		//Generate Foot bar
	 echo '</div></div><div class="w3-bar  ">';

	 echo '<div class="w3-bar-item w3-green" > KM3DIA SW V0.6b '.phpversion().' </div>';
	 
	  $ipaddress = get_client_ip_server();
	echo '<div class="w3-bar-item w3-green" > IP: '. $ipaddress.'</div>' ;
	$Urow=ureader();
	$Loc=QueryILoc();
	if ($Urow["O_Level"]<99)
	{echo '<div class="w3-bar-item w3-green" > User: '.$Urow["Name"]. '  ' .$Urow["Surname"].' '.$Urow["O_Level"].' at '.$Loc.'</div>' ;

		if(isset($_COOKIE["km3bench"])) {
			$B_id=$_COOKIE["km3bench"];
			$Bench=BenchReader($B_id)->fetch_assoc();
		echo '<div class="w3-bar-item w3-green" > Bench: '.$Bench["B_Details"].' </div>' ;
		}
	}
	echo '<img  class="w3-bar-item w3-right  w3-white " id="JJimg"  height="40" width="40" ></img>
	<div  class="w3-bar-item w3-right  w3-blue " id="date">  :  :  </div>';
	
	echo '</div>';
	echo '<script>setInterval(writetime, 1000);</script>';
	if (isset($_COOKIE["km3DIALA_IP"]))
	echo '<script>
		var rpar = Math.floor(Math.random() * 100000) + 1;
		var Out= "https://'.$_COOKIE["km3DIALA_IP"].'/loadimg.php?filetime="+ rpar.toString(5)+
		"&km3DIALA_tkey='.$_COOKIE["KMDIALA_TK"].'&km3user='.$_COOKIE["km3user"].'&km3DIA_add='.GetMyIp().'";
		var NC="https://'.$_SERVER['SERVER_ADDR'].'/NO_CU_CONNECTION.png?filetime="+ rpar.toString(5);
		var res=loadImage(Out, NC );
		console.log(Out);
		
	 </script>';
	 	 
	 }
function GetMyIp ()
{ 	// Returns the real IP address
	// WARNING DEPENDS ON EXTERNAL SERVICE MAY NOT WORK IN FUTURE
	$externalContent = file_get_contents('http://checkip.dyndns.com/');
	preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
	return $m[1];
	
}
function delallcookies()
	{
// Erease all cookies.
	
		if (isset($_SERVER['HTTP_COOKIE'])) 
		{
			$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			foreach($cookies as $cookie) 
			{
				$parts = explode('=', $cookie);
				$name = trim($parts[0]);
				//print_r($name);
				setcookie($name, '', time()-1000);
				setcookie($name, '', time()-1000, '/');
			}
		}
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$cookie_name = "km3lang";
	if(isset($_REQUEST['lang'])){
	$cookie_value = $_REQUEST['lang'];
	} else if(!isset($_COOKIE['lang']))
	{
		$cookie_value ='en';
	}
	setencryptedcookie($cookie_name, $cookie_value);
	}
	}

function checklevel($Urow,$Action)
	{
		$conn = OpenReadConn();
		$sql = "SELECT * FROM PossibleOperations where Description = '$Action';";
		$result = $conn->query($sql);
		$conn->close();
		if ($result->num_rows ==1 ){
			$act = $result->fetch_assoc();	
		} else {
			gotopage('index.php');
			}
			
	//******************	
		if ($Urow["O_Level"]==MONLEVEL) gotopage('index.php'); 
		if ($Urow["O_Level"]>$act["A_level"])
			{
				echo "<BR>Error: User not allowed to do action.<br>";
				echo $Urow["O_Level"]."   ".$act["A_level"];
				sleep(3);
				gotopage('index.php');
			}
	}
function gotopage($page)
	{
			echo '<script type="text/javascript">';
			echo "window.location.href = '$page';</SCRIPT>";
	}

function utter($string,$lang)
	{ 
	/*
	Utter let the system speak the phrase $string with language $lang. 
	Needs the Responsive voice Javascript Lib.
	*/
			$r=rand ();
			echo '<script type="text/javascript">';
			echo 'function utter'.$r.'(){';
			echo 'responsiveVoice.speak("'.$string;
			echo '", "';
			echo $lang.'")';
			echo '}'; 
			echo "</SCRIPT>";
			echo ' <button onclick="utter'.$r.'()" class=" w3-button w3-right"><i class="fa fa-volume-up"></i></button>';
	//https://api.jquery.com/jQuery/#jQuery2
	}
function OccupiedBench($Bench){
	/*Returns the UPI of the part integrated on Bench if occupied or FALSE */
	$con=OpenReadConn();
	$sql="SELECT * FROM ASSIGNED_DOM where B_id='".$Bench."' and SiteID='".$_COOKIE["Ilocation"]."';";
				$result=$con->query($sql);
				$con->close();
				if ($result->num_rows >0 ) {
					$row = $result->fetch_assoc();
					return $row['UPI'];
				}
}
function JumptoBench($UPI)
{
	echo "<FORM id='jumptointegration' method='post' action='startintegration.php'>";
	echo '<input type="hidden"  name="km3action" value="Integration">';
	echo '<input type="hidden" name="DOMUPI" value="'.$UPI.'" readonly>';
	echo "</FORM><script type='text/javascript'>
    document.getElementById('jumptointegration').submit();
</script>";
}
function WorkBenchReader()
	{
		/* Return the list of workbench and DOMUPIs on them
		Bench.B_type=0 AND
		*/
	$con=OpenReadConn();
	
	$sql="SELECT DISTINCT  * FROM Bench LEFT JOIN  ASSIGNED_DOM 
			USING (B_id)  WHERE Bench.B_type=0 AND Bench.SiteID='".$_COOKIE["Ilocation"]."' 
			ORDER BY B_Details;";
	$result=$con->query($sql);
	if ($result->num_rows==0 ){
	 $sql="SELECT  * FROM Bench WHERE  Bench.B_type=0 AND Bench.SiteID='".$_COOKIE["Ilocation"]."' 
			ORDER BY B_Details;";
	$result=$con->query($sql);
	}
	//print_r($sql);
	$con->close();
	return $result;
	}
function BenchReader()
	{/*
	Reads the benchs present in location. OR the details of ONE specific bench i
	*/
		if(isset($_COOKIE["Ilocation"])) {
			$conn = OpenReadConn();
			if (func_num_args() != 0) {
				$args = func_get_args();
				 $sql = "SELECT * FROM Bench where SiteID = '".$_COOKIE["Ilocation"]."' AND B_id=".$args[0].";";
			}else {
				 
				 $sql = "SELECT * FROM Bench where SiteID = '".$_COOKIE["Ilocation"]."';";
			}
			$result = $conn->query($sql);
			$conn->close();
			if ($result->num_rows >0 ){
			return $result;		
			} else {
		//echo "<BR>Error: Bench Not Fond.<br>"; TODO
		//gotopage('index.php');
		//exit;
			}
		}
	}

function findstorage()
	{
	if(isset($_COOKIE["km3location"])) {
			$conn = OpenReadConn();
		if (func_num_args() != 0) { //Call findstorage(0) to find all free work bench  ********** WORKING 
				$args = func_get_args();
				$sql = "SELECT 	be.B_id,be.B_Details FROM Bench be left join ASSIGNED_DOM ad on be.B_id=ad.B_id where be.SiteID = '".$_COOKIE["Ilocation"]."' AND be.B_type=".$args[0]." AND ad.B_id IS NULL;";
			}else {
				 $sql = "SELECT * FROM Bench where SiteID = '".$_COOKIE["Ilocation"]."' AND B_type=1;"; 
			}
			$result = $conn->query($sql);
			$conn->close();
			if ($result->num_rows >0 ) return $result;		
			
		}
	}
function areader($lvl)
	{
	/*
	Action Reader. Only actions that are allowed by User Level Are shown.

	*/
		
			 
		$conn = OpenReadConn();
		$ilvl=intval($lvl);
		$sql = "SELECT * FROM PossibleOperations where A_level > $ilvl;";
		
	// Get the results from DB and store them in a global array 
		$result = $conn->query($sql);
		//global $actions;
		$actions= array();
		if ($result->num_rows >0 ) { 
			while($row = $result->fetch_assoc()) { // fill the PHP array
			if ($row["Visible"])
			array_push($actions, $row["Description"]);
				}
	} else {
		echo "<BR>Error: User Not Logged On.<br>";
		echo '<script type="text/javascript">';
		echo "window.location.href = 'index.php';</SCRIPT>";
		exit;
	}


	$conn->close();
	return $actions;	
	}// End Function Areader
function DecodeScan($String)
{	//If STRING is a part number UPI with a unique position return part  POS to integrate part 
	//If STRING is a Position Return "POS" to be stored for next scan
	//If STRING is a part is PMT return "PMT"
	//In other cases return NULL
	// IT WILL SEARCH ONLY IN THE COMPONENTS TO BE INTEGRATED IN THE ACTUAL PHASE
	//select * from DOMPOSPBS as dp where SUBPART='3.4.5.1.1' and dp.PBS not in (select PBS from DOMPOSPBS where SUBPART='3.4.5.1') ;
	$String=charfilter($String);
	//print_r(" D: ".$String);
	$con=OpenReadConn();
	$sql="Select * From ASSIGNED_DOM as ASD JOIN IntegrationPhase as IPH ON ASD.PhaseID=IPH.PhaseID 
				 where ASD.UPI='".$_COOKIE['DOMUPI']."';";
	//print_r("/n".$sql);
	$result = $con->query($sql);
	if($result) $DestinationRow = $result->fetch_assoc() ; else
				sqlog($sql,$con->error);
	
	$sql="select * from DOMPOSPBS where POS='".$String."' ;";
	$result=$con->query($sql);
	if ($result->num_rows>0) {
		$con->close();
		return 'POS';
	}
	
	if ($DestinationRow['Origin']==NULL)
	{	
	
	$sql="select * from DOMPOSPBS where PBS='".UPI2TOK($String)['PBS']."' AND SUBPART='".UPI2TOK($_COOKIE['DOMUPI'])['PBS']."' ;";

	
	}
	else
	{
	$sql="SELECT * FROM IntegrationPhase where PhaseID='".$DestinationRow['Origin']."'";
	$result = $con->query($sql);
	if($result) $OriginRow = $result->fetch_assoc() ; else sqlog($sql,$con->error);
	  $sql="select * from DOMPOSPBS as dp where PBS='".UPI2TOK($String)['PBS']."'  AND SUBPART='".$DestinationRow['PBS']."' and dp.PBS not in (select PBS from DOMPOSPBS where SUBPART='".$OriginRow['PBS']."') ;";
	}
	//print_r("/n".$sql);
	$result=$con->query($sql);
	if($result) $row=$result->fetch_assoc() ; else sqlog($sql,$con->error);
	//print_r("/n".$row);
	$con->close();
	if ($result->num_rows>0) {
		if ($result->num_rows>1) return 'PMT'; //ANY PART WITH MULTIPLE POSITION 
		return $row['POS'];
	}
	return NULL;
	
}
function QueryP($SUBP,$PBS)
	{// Return if a part is present in a certain SUBPART 
	$con=OpenReadConn();
	$sql="SELECT * FROM DOMPOSPBS where  PBS='".$PBS."' AND SUBPART='".$SUBP."';";
	$result=$con->query($sql);
	$con->close();
	if ($result->num_rows>0) return 1;
	return 0;
	}

	function QueryPBS($Pos)
	{   //returns all Parts present in a Location related to a certain position in DOM.
		$con=OpenReadConn();
		$sql="SELECT a.ISBATCH FROM PBS a JOIN DOMPOSPBS b ON a.PBS=b.PBS and b.POS='".$Pos."';";
		
		if (($con->query($sql)->fetch_assoc())['ISBATCH'])
		{
    $sql="SELECT DISTINCT Parts.PBS,Parts.Variant,Parts.Version,Parts.Serial from Parts INNER JOIN DOMPOSPBS ON Parts.PBS=DOMPOSPBS.PBS 
				  INNER JOIN Bench on Parts.B_id=Bench.B_id AND Bench.SiteID = '".$_COOKIE["Ilocation"]."' 
				  AND DOMPOSPBS.POS='".$Pos."' ;" ;
	// OK FOR UNIQUE NOT FOR BATCH	
		} else {
		$sql="SELECT DISTINCT a.* from Parts a  LEFT JOIN DOM_INTEGRATION d ON a.PARTUPI=d.PRODUPI JOIN DOMPOSPBS b ON a.PBS=b.PBS 
				   JOIN Bench c on a.B_id=c.B_id  where c.SiteID = '".$_COOKIE["Ilocation"]."' 
				  AND b.POS='".$Pos."' AND  d.PRODUPI IS NULL;" ; 
		}
		//print_r($sql);
		$result=$con->query($sql);
		$con->close();
		return $result;
	}

function ureader($Uoid="")
	{
	/*
	User Reader
	*/
			$Urow= array();
			$Urow["O_Level"]=100; //Set Default UserLevel to 100 (NO User)
		if(isset($_COOKIE["km3user"])) $Uoid=$_COOKIE["km3user"];
		if(isset($_COOKIE["km3user_MON"])) $Uoid=$_COOKIE["km3user_MON"];
		//{		
			if ($Uoid!="")
			 
			 {
			$conn = OpenReadConn();


			$sql = "SELECT * FROM Operators where KM3db_OID = '$Uoid';";
			$result = $conn->query($sql);
			$conn->close();
		if ($result->num_rows == 1) { // Only One person Logon
			$Urow = $result->fetch_assoc();
			$conn = OpenReadConn();
			$sql = "SELECT * FROM Site where KM3db_LOC_ID = '".$Urow["KM3db_LOC_ID"]."';";
			
			$result = $conn->query($sql);
			$Lrow = $result->fetch_assoc();
			$conn->close();
			
		if ($Lrow['SiteID']== NULL)
				{
				echo "<BR>Error: User NOT registred at integratin Site.<br>";
				echo '<script type="text/javascript">';
				echo "window.location.href = 'index.php';</SCRIPT>";
				exit;
				}
		if (!isset($_COOKIE["O_Type"])) {
				$cookie_name = "O_Type";
				$cookie_value = $Lrow['O_Type'];
				setencryptedcookie($cookie_name, $cookie_value);
				}
		if (!isset($_COOKIE["Ilocation"])) {
				$cookie_name = "Ilocation";
				$cookie_value = $Lrow['SiteID'];
				
				setencryptedcookie($cookie_name, $cookie_value);
				}
	   if (!isset($_COOKIE["km3location"])) {
				$cookie_name = "km3location";
				$cookie_value = $Urow["KM3db_LOC_ID"];
				setencryptedcookie($cookie_name, $cookie_value);
				}
			} else {
				echo "<BR>Error: User Code not found.<br>";
				echo '<script type="text/javascript">';
				echo "window.location.href = 'index.php';</SCRIPT>";
				exit;
				}
		
		 }
		/*else {
		echo "<BR>Error: KM3DIA SW Needs Cookies to work. No User name found.<br>";
		}*/
	return $Urow;
				}// End Function ureader

function langreader($Page) 
	{   
		if(isset($_COOKIE["km3lang"])) {
			
			 $ulang=$_COOKIE["km3lang"];
	 

	 //global $ht_string ;
	 
	 $ht_string = array();
	// Create connection
	$conn_lang = new mysqli("localhost", apache_getenv("MYSQL_LANGU"), apache_getenv("MYSQL_LANGP"), "KM3LANG");
	//$conn->set_charset ( 'utf-8' );
	// Check connection
	if ($conn_lang->connect_error) {
		die("Connection failed: " . $conn_lang->connect_error);
		echo "Failed to connect to site strings Database";
	} 

	$sql_lang = "SELECT * from Strings where Lang='$ulang' AND Page_id=$Page ORDER BY USI";
	$sql_voice = "SELECT * from VOICES where Lang='$ulang'";
	$result_lang = $conn_lang->query($sql_lang);
	$result_Voice = $conn_lang->query($sql_voice);
	if ($result_lang->num_rows > 0) {
		// output data of each row
		$ht_string=array_fill(0,$result_lang->num_rows,"");
		while($row = $result_lang->fetch_assoc()) { // fill the PHP array
			
			
			$ht_string[$row["STR_order"]]=utf8_encode ( $row["STRING"] );
			/* for php<7.2
			$ht_string[$row["STR_order"]]= iconv("ISO-8859-1", "UTF-8//IGNORE", $row["STRING"]);
			*/
		}
	} else {
		echo '<script type="text/javascript">';
		echo "window.location.href = 'logout.php';</SCRIPT>";
	}
	if ($result_Voice->num_rows>0) {
		$row = $result_Voice->fetch_assoc();
		
		$ht_string[0]=utf8_encode ( $row["Description"] );
	}
	
	$conn_lang->close();
	return $ht_string;
	} else {
		echo "<BR>Error: KM3DIA SW Needs Cookies to work. No Language Found.<br>";
	  
	}
	}
function get_client_ip_server() {
		$ipaddress = '';
		if($_SERVER['REMOTE_ADDR'])
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';
	 
		return $ipaddress;
	}
	function get_client_ip_env() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
	 
		return $ipaddress;
}
function sqlog($sql,$Urow)
{   /*
	Keep log of operations on Db.
	*/
	//$comment='#User: '.$Urow["KM3db_OID"].' '.$Urow["Name"]. '  ' .$Urow["Surname"].' '.date(DATE_RFC2822);
	$comment='# '.$Urow.'#Date:   '.date(DATE_RFC2822);
	
    $comment=$comment.PHP_EOL;
	$sql=$sql.PHP_EOL;
	file_put_contents('/var/log/Km3DIA_Sql_Log.mysql', $comment, FILE_APPEND | LOCK_EX);
	file_put_contents('/var/log/Km3DIA_Sql_Log.mysql', $sql, FILE_APPEND | LOCK_EX);
}	

function BenchCode2TOK($BenchCode)
{	/* 
	Decode Bench QR Code in B_id and KM3db_LOC_ID
	*/
	if (strlen ( $BenchCode ) < 8) // TO DO MAKE A BETTER Check ! 9999.99 
	{
	$BenchCode_Tok = array();
	$tok = strtok($BenchCode, ".");
	$BenchCode_Tok['B_id']=$tok;
	$tok = strtok(".");
	$BenchCode_Tok['KM3db_LOC_ID']=$tok;
	return $BenchCode_Tok;
	}
}	

function Userkey2TOK($UserCode)
{	/* 
	Decode User QR Code in Skey and KM3db_User_ID
	*/
	$UserCode_Tok = array();
	$tok = strtok($UserCode, ".");
	$UserCode_Tok['Skey']=$tok;
	$tok = strtok(".");
	$UserCode_Tok['KM3db_OID']=$tok;
	return $UserCode_Tok;
}
function SFP2TOK($Sfpvariant)
{	/* 
SFP 3.4.3.2.3.1/WL-1561.01:CH20H:DOM/1.625
DOM 3.4/CH20H/3.34 
	Decode SFP VARIANT
	
	preg_match('/CH[1234567890]{2}[HC]/',$Sfpvariant);

	*/ 				
	$Sfp_Tok = array();
	$tok = strtok($Sfpvariant, ":");
	$Sfp_Tok['Generic']=$tok;
	$tok = strtok(":");
	$Sfp_Tok['Ch']=$tok;
	return $Sfp_Tok;
}
function charfilter($String)
{   /*
	Sanitize input
	*/
	$count=0;
	$forbidden= array("'",";","--","=","\"","#","<",">");
	$String=str_replace($forbidden," ", $String,$count);
	
	// String lenght limited to MAX_BUFF
	return substr($String,0,MAX_BUFF);
}
function GenRSoid($domupi)
{
	/*
	Generate an RSOID for one DOM using Version and Serial
	*/
	return "KM3DIA123";
}
function UPI2TOK($Upi)
{	/*
	Sanitize input
	*/
	
	$Upi=charfilter($Upi);
	
	/* 
	Decode UPI 
	*/
	$UPI_Tok = array();
	$tok = strtok($Upi, "/");
	//if (VerifyPBS($tok)->num_rows ==1)
	//{
	$UPI_Tok['PBS']=$tok;
	$tok = strtok("/");
	
	$UPI_Tok['VARIANT']=$tok;
	$tok = strtok(".");
	
	$UPI_Tok['VERSION']=$tok;
	$tok = strtok(".");
	
	$UPI_Tok['SERIAL']=$tok;
	//}
	
    return $UPI_Tok;
/*TODO Check if UPI is valid
3.4.3.2.3.1/WL-1561.01:CH20H:DOM/1.625
*/
}	
function OpenReadConn()
{
	//Opens a read olny connection with local DataBase
	$conn = new mysqli("localhost", apache_getenv("MYSQL_USER"), apache_getenv("MYSQL_R_PW"), "KM3DIA");
	$conn->set_charset ( 'utf-8' );
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	return $conn;
}
function OpenWriteConn()
	{	$conn = new mysqli("localhost", apache_getenv("MYSQL_WR_USER"), apache_getenv("MYSQL_WR_PW"), "KM3DIA");
		$conn->set_charset ( 'utf-8' );
		if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
		}
		return $conn;
	}
function FindParts()
	{
		if(isset($_COOKIE["Ilocation"])) {
			$conn = OpenReadConn();
			if (func_num_args() != 0) {
				$args = func_get_args();
				$UPI_tok=UPI2TOK($args[0]);
				$sql="SELECT *  FROM Parts INNER JOIN Bench on Parts.B_id=Bench.B_id WHERE   Parts.PBS LIKE '".$UPI_tok["PBS"]."' AND Parts.Variant LIKE '".$UPI_tok["VARIANT"].
						"' AND Parts.Version LIKE '".$UPI_tok["VERSION"]."' AND Parts.Serial LIKE '".$UPI_tok["SERIAL"]."' AND Bench.SiteID='".$_COOKIE["Ilocation"]."';";
					} else {//Generic query with NO arguments. A list of all components at location will be generated.
						$sql="SELECT *  FROM Parts INNER JOIN Bench on Parts.B_id=Bench.B_id WHERE Bench.SiteID='".$_COOKIE["Ilocation"]."';";
					}
				$result = $conn->query($sql);
				$conn->close();
				return $result;
					}
				
	}
function QueryParts()
	{
		if(isset($_COOKIE["Ilocation"])) {
			$conn = OpenReadConn();
			if (func_num_args() != 0) {
				$args = func_get_args();
				$UPI_tok=UPI2TOK($args[0]);
				//Corrected the BATCH check
			if (IsBatch($UPI_tok["PBS"])&&func_num_args()<2)
					{ //IS BATCH So you CAN have the same UPI in two or more different LOCATIONS 
					$sql="SELECT Bench.SiteID  FROM Bench INNER JOIN Parts on Parts.B_id=Bench.B_id WHERE  Bench.SiteID='".$_COOKIE["Ilocation"]."' AND Parts.PBS LIKE '"
						.$UPI_tok["PBS"]."' AND Parts.Variant LIKE '".$UPI_tok["VARIANT"].
						"' AND Parts.Version LIKE '".$UPI_tok["VERSION"]."' AND Parts.Serial LIKE '".$UPI_tok["SERIAL"]."';";
					} else 
					{ // Is NOT Batch so UPI should be unique. SELECT B_id  FROM Parts
						$sql="SELECT *  FROM Parts INNER JOIN Bench on Parts.B_id=Bench.B_id WHERE   Parts.PBS LIKE '".$UPI_tok["PBS"]."' AND Parts.Variant LIKE '".$UPI_tok["VARIANT"].
						"' AND Parts.Version LIKE '".$UPI_tok["VERSION"]."' AND Parts.Serial LIKE '".$UPI_tok["SERIAL"]."';";
					}
					} else {//Generic query with NO arguments. A list of all components at location will be generated.
						$sql="SELECT *  FROM Parts INNER JOIN Bench on Parts.B_id=Bench.B_id WHERE Bench.SiteID='".$_COOKIE["Ilocation"]."';";
					}
					}
				$result = $conn->query($sql);
				$conn->close();
				return $result;
	}
function VerifyPart()
	{ // Search for an EXACT match
		if(isset($_COOKIE["Ilocation"])) {
			$conn = OpenReadConn();
			if (func_num_args() != 0) {
				$args = func_get_args();
				$UPI_tok=UPI2TOK($args[0]);
				
			$sql="SELECT *  FROM Parts INNER JOIN Bench on Parts.B_id=Bench.B_id WHERE   Parts.PBS = '".$UPI_tok["PBS"]."' AND Parts.Variant = '".$UPI_tok["VARIANT"].
						"' AND Parts.Version = '".$UPI_tok["VERSION"]."' AND Parts.Serial = '".$UPI_tok["SERIAL"]."' AND Bench.SiteID='".$_COOKIE["Ilocation"]."';";
				
				$result = $conn->query($sql);
				$conn->close();
				return $result;
	}
	}}
	
function QueryDomPart()
	{
		if(isset($_COOKIE["Ilocation"])) {
			$conn = OpenReadConn();
			if (func_num_args() != 0) {
				$args = func_get_args();
				$args[0]=charfilter($args[0]);//SANITIZE ARG[0] Just in case
				$UPI_tok=UPI2TOK($args[0]); 
				
				if ($UPI_tok["PBS"]=="3.4"&&func_num_args()<2)//BUG 3.4 ONLY FOR DOM
				{
				
		   $sql="SELECT DISTINCT  * FROM Bench LEFT JOIN  ASSIGNED_DOM ad
			USING (B_id)  (ad.KM3db_LOC_ID IS NULL OR ad.Repair=1) AND Bench.SiteID='".$_COOKIE["Ilocation"].
			"' AND UPI LIKE '".$args[0]."';";	
				} else { 
			$sql="SELECT   * FROM Bench LEFT JOIN  ASSIGNED_DOM ad
			USING (B_id) LEFT JOIN DOM_INTEGRATION ON ad.DOMUPI=ASSIGNED_DOM.UPI WHERE  Bench.SiteID='".$_COOKIE["Ilocation"]."' 
			   AND DOM_INTEGRATION.PRODUPI LIKE '".$args[0]."';";
					
				}
			} else 
			{
				$sql="SELECT DISTINCT  * FROM Bench LEFT JOIN  ASSIGNED_DOM ad
			USING (B_id)  WHERE (ad.KM3db_LOC_ID IS NULL or ad.Complete=1 or ad.Repair=1) AND Bench.SiteID='".$_COOKIE["Ilocation"]."';";
			
			}   
				$result = $conn->query($sql);
				if(!$result) sqlog($sql,$conn->error);
				$conn->close();
				return $result;
				
				
		}
	}
function QueryMerge($Upi)
{
	$UPI_tok=UPI2TOK($Upi);
	$conn = OpenReadConn();
	$sql="select * from IntegrationPhase as ip left join IntegrationPhase as 
	iipp on   ip.PBS=iipp.PBS where ip.PBS='".$UPI_tok['PBS']."' and ip.Origin=ip.PhaseID ;";
	$result = $conn->query($sql);
	if(!$result) sqlog($sql,$conn->error);
	if ($result->num_rows >0 ) // Number of ROWS should be 1 in this model
		{
		$row = $result->fetch_assoc();
		$sql="select * from IntegrationPhase where Destination='".$row['PhaseID']."';";
		$result = $conn->query($sql);
		if(!$result) sqlog($sql,$conn->error);
		}
	$conn->close();
	return $result; 
}
	function QueryDom()
	{
/*
Search for Assigned DOMS (not sub parts) at the login location ($_COOKIE["Ilocation"])

*/
		if(isset($_COOKIE["Ilocation"])) {
			$conn = OpenReadConn();
			if (func_num_args() != 0) {
				$args = func_get_args();
				$UPI_tok=UPI2TOK($args[0]); //SANITIZE ARG[0] Just in case
				
				if ($UPI_tok["PBS"]=="3.4"&&func_num_args()<2)
				{
				$sql = "SELECT * FROM ASSIGNED_DOM INNER JOIN Site ON  Site.KM3db_LOC_ID=ASSIGNED_DOM.KM3db_LOC_ID 
				where Site.SiteID='".$_COOKIE["Ilocation"].
			    "' AND UPI LIKE '".$args[0]."';";
				}
			} else 
			{
				$sql = "SELECT * FROM ASSIGNED_DOM INNER JOIN Site ON  Site.KM3db_LOC_ID=ASSIGNED_DOM.KM3db_LOC_ID 
				where Site.SiteID='".$_COOKIE["Ilocation"]."';";
			}
				$result = $conn->query($sql);
				$conn->close();
				return $result;
		}
	}
	
function QueryILoc()
	{   // Return Current Integration Location
		if(isset($_COOKIE['Ilocation'])){
		$conn=OpenReadConn();
		$sql = "SELECT * from ISites where SiteID='".$_COOKIE['Ilocation']."';";
		$result = $conn->query($sql);
		if ($result->num_rows > 0)
		{
			$row = $result->fetch_assoc();
			return $row["Town"];
		}
		$conn->close();
		}
	}
	// 3.4.1.1/DEFAULT/1.1 Test BATCH UPI
	// 3.4.2.3/HAMA-R12199/1.12648 TEST UNIQUE UPI 
function VerifyPBS($UPI)
		{
				$conn=OpenReadConn();
				$UPI_tok=UPI2TOK($UPI);
				$sql="SELECT * FROM PBS where PBS='".$UPI_tok["PBS"]."'";
				$result = $conn->query($sql);
				$conn->close();
				return $result;
		}
function AddPart()
	{/*
	Todo: insert special "pages" for messages from functions
	*/if (func_num_args() != 0) {
				$args = func_get_args();
				$UPI=$args[0];
				$UPI_tok=UPI2TOK($UPI);
				$result = VerifyPBS($UPI);
				if ($result!=NULL)
				if ($result->num_rows >0) {
					while ($row = $result->fetch_assoc())
					$message=$row["PBS"]." : ".$row["Description"]."<br>";
				
					 $result = QueryParts($UPI);
					 if ($result->num_rows >0){
						 $message=$message."".$UPI." Already in DB.";
					 } else 
					 {
						 $conn=OpenWriteConn();
						 $sql="INSERT INTO Parts (PBS,Variant,Version,Serial,B_id) VALUES('"
						 .$UPI_tok["PBS"]."','".$UPI_tok["VARIANT"]."','".$UPI_tok["VERSION"]."','"
						 .$UPI_tok["SERIAL"]."' , '".$_COOKIE["Ilocation"]."');";
						 $result = $conn->query($sql);
						 //print_r($sql);
						 $conn->close();
					 }
					 if (func_num_args() < 2)
					 ShowMessage($message);
					/*if ($row["VARIANT"]=="DEFAULT")
					{ //IS BATCH
					}*/
					
				} 
				
		}
	}
function CountEmptyPos($UPI)
 {
			$conn=OpenReadConn();
			$sql="SELECT ((SELECT COUNT(*)  FROM DOM_INTEGRATION WHERE  DOMUPI='".$UPI."') 
				-(SELECT COUNT(*)  FROM DOM_INTEGRATION WHERE PRODUPI IS NOT NULL AND DOMUPI='".$UPI."')) as empty_pos ,
				 (SELECT COUNT(*)   FROM DOM_INTEGRATION WHERE  DOMUPI='".$UPI."') as all_pos ;";
				$result= $conn->query($sql);
				$row = $result->fetch_assoc();
				
				
				 if ($row['all_pos'] >0){
				$conn->close();
				
				return $row;
				 } else {
				    
					 $sql="SELECT (SELECT 0 AS  empty_pos), (SELECT 10 AS all_pos);";
					 $result= $conn->query($sql);
					$conn->close();
					$row = $result->fetch_assoc();
					//print_r($row);
					return $row;
				 }
}
function ClosePartIntegration($UPI)
{ // Make parts NOT modifiable 
				$progress=CountEmptyPos($UPI);
				// SHOULD GIVE AN ALERT IL FAIL
				if($progress['empty_pos']==0&&$progress['all_pos']>0)
				{
				$conn=OpenWriteConn();
				$sql="UPDATE DOM_INTEGRATION SET Modifiable=0 where DOMUPI='".$UPI."';";
				$result = $conn->query($sql);
				if(!$result){
					sqlog($sql,$conn->error);
					$conn->close();
					return 0;
				} 
				$sql="UPDATE ASSIGNED_DOM SET Complete=1 where UPI='".$UPI."';";
				$result = $conn->query($sql);
				if(!$result){
					sqlog($sql,$conn->error);
					$conn->close();
					return 0;
				} 
				$conn->close();
				return 1; // 1 means OK DONE.
				}	
				return 0;
}
function SetNotConform($UPI)
{				ClosePartIntegration($UPI);
				$conn=OpenWriteConn();
				$sql="UPDATE ASSIGNED_DOM SET NotConform=1 where UPI='".$UPI."';";
				$result = $conn->query($sql);
				$conn->close();
				return 1;
}
function ResetNotConform($UPI)
{				
				$conn=OpenWriteConn();
				$sql="UPDATE ASSIGNED_DOM SET NotConform=0 where UPI='".$UPI."';";
				$result = $conn->query($sql);
				$conn->close();
				return 1;
}
function QueryConform($UPI)
{				
				$conn=OpenReadConn();
				$sql="SELECT NotConform FROM ASSIGNED_DOM WHERE UPI='".$UPI."';";
				$result = $conn->query($sql);
				$conn->close();
				if($result)
				{$row=$result->fetch_assoc();
				  return $row['NotConform'];
				} else {return NULL;}
}
function UnlockPartIntegration($UPI)
{ // Make parts  modifiable //// VERSION 0 NO CHECKS !!!!
				
				$conn=OpenWriteConn();
				$sql="UPDATE DOM_INTEGRATION SET Modifiable=1 where DOMUPI='".$UPI."';";
				$result = $conn->query($sql);
				$sql="UPDATE ASSIGNED_DOM SET Complete=0 where UPI='".$UPI."';";
				$result = $conn->query($sql);
				$conn->close();
					
}
function ActualPhase($UPI)
{
		$UpiTok=UPI2TOK($UPI);
		$con=OpenReadConn();
		$sql="SELECT * FROM IntegrationPhase WHERE PBS='".$UpiTok['PBS']."';";
		$result=$con->query($sql);
		if ($result) $Iph=$result->fetch_assoc(); //TODO make better error handling here
		$con->close();
		return $Iph;
}

function CreateNewDomPart($UPI,$B_id="")
{
	if ($B_id=="") $B_id=$_COOKIE["km3bench"];
	$PhaseID=ActualPhase($UPI)['PhaseID'];
		
		$con=OpenWriteConn();
	// Creates record in ASSIGNED_DOM table if not existing
	    $sql="SELECT * FROM ASSIGNED_DOM WHERE  UPI='".$UPI."';";
		$result = $con->query($sql);
		if ( $result->num_rows==0)
		{
		$sql="INSERT INTO ASSIGNED_DOM (UPI,SiteID,B_id,PhaseID) VALUES ('".$UPI."','".$_COOKIE["Ilocation"]."','".$B_id."','".$PhaseID."')";
		$result = $con->query($sql);
		if(!$result)sqlog($sql,'NON CREA IL RECORD'.$conn->error);
		}
		$con->close();
}
function CreateNewPart($UPI,$B_id="")
{		/*  ADD NEW PART  TO DOM_INTEGRATION TABLE */
		if ($B_id=="") $B_id=$_COOKIE["km3bench"];
		$SiteID=$_COOKIE["Ilocation"];
		$UpiTok=UPI2TOK($UPI);
		CreateNewDomPart($UPI,$B_id); // Creates record in ASSIGNED_DOM table
		$con=OpenWriteConn();
		// Creates the empty records for integration.
			$sql="SELECT * FROM DOMPOSPBS where SUBPART='".$UpiTok['PBS']."' order by Id ";
			$result = $con->query($sql); // This query could result to NULL  
		if ($result)
		if ( $result->num_rows>0)
		{ // If the new subpart have new parts to be integrated
		  // creates new rows in the DOM_INTEGRATION TABLE
			$row = $result->fetch_assoc();
			$sql="INSERT INTO DOM_INTEGRATION (DOMUPI,POS,B_id) VALUES 
			     ('".$UPI."','".$row['POS']."','".$B_id."')";
			while($row = $result->fetch_assoc()) { 
			$sql=$sql.",('".$UPI."','".$row['POS']."','".$B_id."')";
				}
			$sql=$sql.";";
			$result=$con->query($sql);
		}
		$con->close();

}

function Merge($UPI1,$UPI2,$SiteID,$B_id)
{
		if(hasclb($UPI1))
	{
		$serial=UPI2TOK($UPI1)['SERIAL'];
	} else {
		$serial=UPI2TOK($UPI2)['SERIAL'];
	}
				$conn=OpenReadConn();
				$sql="SELECT * FROM ASSIGNED_DOM where UPI='".$UPI1."';";
				$result = $conn->query($sql);
				if($result) $PartRow1 = $result->fetch_assoc();
				$sql="SELECT * FROM ASSIGNED_DOM where UPI='".$UPI2."';";
				$result = $conn->query($sql);
				if($result) $PartRow2 = $result->fetch_assoc();
				
				$sql="SELECT * FROM IntegrationPhase where PhaseID='".$PartRow1['PhaseID']."';";
				$result = $conn->query($sql);
				if($result) $PhaseRow1 = $result->fetch_assoc() ; else
				sqlog($sql,$conn->error);
				
				$sql="SELECT * FROM IntegrationPhase where PhaseID='".$PartRow2['PhaseID']."';";
				$result = $conn->query($sql);
				if($result) $PhaseRow2 = $result->fetch_assoc(); else
				sqlog($sql,$conn->error);
				
				$sql="SELECT * FROM IntegrationPhase where PhaseID='".$PhaseRow1['Destination']."';";
				$result = $conn->query($sql);
				if($result) $DestinationRow = $result->fetch_assoc(); else
				sqlog($sql,$conn->error);
				$conn->close();
///////// SFP SPECIAL CODE //
				$variant=UPI2TOK($UPI1)['VARIANT'];
				if (preg_match('/CH[1234567890]{2}[HC]/',UPI2TOK($UPI1)['VARIANT']))
					{
					$variant=UPI2TOK($UPI1)['VARIANT'];
					} else
				if (preg_match('/CH[1234567890]{2}[HC]/',UPI2TOK($UPI2)['VARIANT']))
					{
					$variant=UPI2TOK($UPI2)['VARIANT'];
					}
				
				//if (strlen(UPI2TOK($UPI1)['VARIANT'])==5)$variant=UPI2TOK($UPI1)['VARIANT'];
				//if (strlen(UPI2TOK($UPI2)['VARIANT'])==5)$variant=UPI2TOK($UPI2)['VARIANT'];
				if ($variant=="")
					$newupi=GenUniqUPI($DestinationRow['PBS'],$serial);
				else
				{
					$newupi=$DestinationRow['PBS']."/".$variant."/".$_COOKIE['Ilocation'].".".$serial;
				}
//////// END SFP //////////
				
				CreateNewDomPart($newupi);
				
				$conn=OpenWriteConn();
				/*
				$sql="UPDATE Parts SET IsPartof='".$newupi."' WHERE IsPartof='".$UPI1."' OR IsPartof='".$UPI2."' ;"; 
				$result = $conn->query($sql);
				sqlog($sql,mysqli_affected_rows($conn).' COLONNE PART');
				
				if(!$result)sqlog($sql,'NON FA IL UPDATE PARTS'.$conn->error);
				*/
				$sql="UPDATE DOM_INTEGRATION SET DOMUPI='".$newupi."' where DOMUPI='".$UPI1."' OR DOMUPI='".$UPI2."' ;";
				//$sql="UPDATE DOM_INTEGRATION as d1 inner join DOM_INTEGRATION as d2 on (d1.POS=d2.POS and  d1.DOMUPI='".$newupi."' and d2.DOMUPI='".$UPI1."') SET d1.PRODUPI=d2.PRODUPI ;";
				$result = $conn->query($sql);
				sqlog($sql,mysqli_affected_rows($conn).' COLONNE1');
				if(!$result)sqlog($sql,'NON FA IL UPDATE DOM_INTEGRATION 1 :'.$conn->error);
				/*
				$sql="UPDATE DOM_INTEGRATION as d1 inner join DOM_INTEGRATION as d2 on (d1.POS=d2.POS and  d1.DOMUPI='".$newupi."' and d2.DOMUPI='".$UPI2."') SET d1.PRODUPI=d2.PRODUPI ;";
				$result = $conn->query($sql);
				sqlog($sql,mysqli_affected_rows($conn).' COLONNE2');
				if(!$result)sqlog($sql,'NON FA IL UPDATE DOM_INTEGRATION 2 :'.$conn->error);
*/
				
				$sql="UPDATE Test SET UPI='".$newupi."' where UPI='".$UPI1."' OR UPI='".$UPI2."' ;";
				$result = $conn->query($sql);
				sqlog($sql,mysqli_affected_rows($conn).' COLONNE TEST');

				if(!$result)sqlog($sql,'NON FA IL UPDATE Test :'.$conn->error);
		
				
				$sql="DELETE FROM ASSIGNED_DOM WHERE UPI='".$UPI1."' OR UPI='".$UPI2."';";
				$result = $conn->query($sql);
				if(!$result)sqlog($sql,$conn->error);
				
				
				$conn->close();
				return "{km3action: 'integration',DOMUPI: '".$newupi."', PhaseID: '".$DestinationRow['PhaseID']."',EV: '1'}";

				
}
function Couple($UPI1)
{	if (isset($_COOKIE['DOMUPI'])) $UPI2=$_COOKIE['DOMUPI'];
	if(hasclb($UPI1))
	{
		$serial=UPI2TOK($UPI1)['SERIAL'];
		$variant="CP:".UPI2TOK($UPI1)['VARIANT'];
	} else {
		$serial=UPI2TOK($UPI2)['SERIAL'];
		$variant="CP:".UPI2TOK($UPI2)['VARIANT'];
	}
	$conn=OpenReadConn();
	$sql="Select * From ASSIGNED_DOM as ASD JOIN IntegrationPhase as IPH ON ASD.PhaseID=IPH.PhaseID 
				JOIN IntegrationPhase AS DEST ON IPH.Destination=DEST.PhaseID where ASD.UPI='".$UPI2."';";
				$result = $conn->query($sql);
				if($result) $DestinationRow = $result->fetch_assoc() ; else
				sqlog($sql,$conn->error);
	$conn->close();
	
	//$variant="CP:".UPI2TOK($UPI1)['VARIANT']; //Default Variant Using UPI1 to keep it unique.
	if (preg_match('/CH[1234567890]{2}[HC]/',UPI2TOK($UPI1)['VARIANT'])) //Phase 1
					$variant="CP:".UPI2TOK($UPI1)['VARIANT'];
	if (preg_match('/CH[1234567890]{2}[HC]/',UPI2TOK($UPI2)['VARIANT'])) //Phase 1
					$variant="CP:".UPI2TOK($UPI2)['VARIANT'];
					
	$newupi1=UPI2TOK($UPI1)['PBS']."/".$variant."/".UPI2TOK($UPI1)['VERSION'].".".$serial;
	$newupi2=UPI2TOK($UPI2)['PBS']."/".$variant."/".UPI2TOK($UPI2)['VERSION'].".".$serial;
	/*
	print_r($UPI1);
	print_r('___');
	print_r($newupi1);
	print_r('___');
	print_r($UPI2);
	print_r(' ___ ');
	print_r($newupi2);
	*/
	
	ChangeUPI($UPI1,$newupi1);
	ChangeUPI($UPI2,$newupi2);
	return "{km3action: 'integration',DOMUPI: '".$newupi2."', PhaseID: '".$DestinationRow['PhaseID']."',EV: '1'}";

	
}
function CoupleNextPhase($UPI)
{ // Check if next phase is a Coupling

	if (!(preg_match("/^(CP:).*/",UPI2TOK($UPI)['VARIANT']))) 
	{
		$conn=OpenReadConn();
        $sql="Select * From ASSIGNED_DOM as ASD JOIN IntegrationPhase as 
		 IPH ON ASD.PhaseID=IPH.PhaseID where ASD.UPI='".$UPI."';";
				$result = $conn->query($sql);
				if($result) $DestinationRow = $result->fetch_assoc() ; else
				sqlog($sql,$conn->error);
		$sql="Select * From IntegrationPhase where PhaseID=-".$DestinationRow['Destination']." ;";
		$res = $conn->query($sql);
		if($res) $DestinationRow = $res->fetch_assoc() ; else
				sqlog($sql,$conn->error);
		
				$conn->close();
		if ($res)
		return ($res ->num_rows >0);
	}
	return (0);
}
function MergeNextPhase($UPI)
{ // Check if next phase is a MERGE
				$conn=OpenReadConn();
				
				$sql="Select * From ASSIGNED_DOM as ASD JOIN IntegrationPhase as IPH ON ASD.PhaseID=IPH.PhaseID 
				JOIN IntegrationPhase AS DEST ON IPH.Destination=DEST.PhaseID where ASD.UPI='".$UPI."';";
				$result = $conn->query($sql);
				if($result) $DestinationRow = $result->fetch_assoc() ; else
				sqlog($sql,$conn->error);
				//print_r($DestinationRow);
				$conn->close();
		return ($DestinationRow['PhaseID']==$DestinationRow['Origin']);
}
 
function QueryCoupled($UPI)
{
	/*
	Search for Partial PARTS already coupled with current.

	*/
	if (!(preg_match("/^(CP:).*/",UPI2TOK($UPI)['VARIANT'])))
				return false;
				$conn=OpenReadConn();
				$UPITOK=UPI2TOK($UPI);
				$sql="SELECT * FROM ASSIGNED_DOM WHERE UPI LIKE '%/".$UPITOK['VARIANT']."/%' AND
				 UPI NOT LIKE '".$UPITOK['PBS']."/%';";
				$result = $conn->query($sql);
				$conn->close();
				if(!$result) sqlog($sql,$conn->error) ; 
				
				return 	$result;
					
}

function ChangeUPI($UPI,$newupi,$B_id="")
{
	if ($B_id=="") $B_id=$_COOKIE["km3bench"];
	if ($B_id=="") return;
	// Change the DOM subpart registered UPI with a new one.
	  if($UPI==$newupi) return;
				CreateNewPart($newupi,$B_id);
				$conn=OpenWriteConn();
				$sql="SELECT * FROM ASSIGNED_DOM WHERE UPI='".$UPI."' ;";
				$result = $conn->query($sql);
				$row = $result->fetch_assoc();
				
				if($row[KM3db_LOC_ID]=='')
				{
			    $sql="UPDATE ASSIGNED_DOM SET SiteID='".$row[SiteID]."',PhaseID='".$row[PhaseID]."',B_id='".$row[B_id]."',Complete='".$row[Complete]."',NotConform='".$row[NotConform]."' where UPI='".$newupi."';";
				}else{
				$sql="UPDATE ASSIGNED_DOM SET KM3db_LOC_ID= '".$row[KM3db_LOC_ID]."',SiteID='".$row[SiteID]."',PhaseID='".$row[PhaseID]."',B_id='".$row[B_id]."',Complete='".$row[Complete]."',NotConform='".$row[NotConform]."' where UPI='".$newupi."';";
				}
				$result = $conn->query($sql);
				if(!$result)sqlog($sql,'NON FA IL UPDATE ASSIGNED_DOM'.$conn->error);

				$sql="UPDATE Test SET UPI='".$newupi."' where UPI='".$UPI."';";
				$result = $conn->query($sql);
				if(!$result)sqlog($sql,'NON FA IL UPDATE Test :'.$conn->error);
				
				$sql="UPDATE IntLog SET DOMUPI='".$newupi."' where DOMUPI='".$UPI."';";
				$result = $conn->query($sql);
				if(!$result)sqlog($sql,'NON FA IL UPDATE Logbook :'.$conn->error);
				
				$sql="UPDATE DOM_INTEGRATION as d1 inner join DOM_INTEGRATION as d2 on (d1.POS=d2.POS and  d1.DOMUPI='".$newupi."' and d2.DOMUPI='".$UPI."') SET d1.PRODUPI=d2.PRODUPI, d1.B_id=d2.B_id;";	
				$result = $conn->query($sql);
				if(!$result)sqlog($sql,'NON FA IL UPDATE DOM_INTEGRATION :'.$conn->error);
				
				$sql="DELETE FROM DOM_INTEGRATION WHERE DOMUPI='".$UPI."';";
				$result = $conn->query($sql);
				if(!$result)sqlog($sql,'NON FA IL DELETE DOM_INTEGRATION :'.$conn->error);
				
				$sql="DELETE FROM ASSIGNED_DOM WHERE UPI='".$UPI."';";
				$result = $conn->query($sql);
				if(!$result)sqlog($sql,'NON FA IL DELETE ASSIGNED_DOM :'.$conn->error);
				
				$conn->close();
				//WriteLogbook("CHANGE UPI",$UPI." CHANGED TO :".$newupi,"",$B_id);
					
}

function Evolve($UPI,$SiteID,$B_id)
{
			//if (ClosePartIntegration($UPI))
				{
				$conn=OpenReadConn();
				/*
				Verify if next phase is MERGE
				*/
				$sql="Select * From ASSIGNED_DOM as ASD JOIN IntegrationPhase as IPH ON ASD.PhaseID=IPH.PhaseID 
				JOIN IntegrationPhase AS DEST ON IPH.Destination=DEST.PhaseID where ASD.UPI='".$UPI."';";
				$result = $conn->query($sql);
				if($result) $DestinationRow = $result->fetch_assoc() ; else
				sqlog($sql,$conn->error);
				
				$conn->close();
				
				if ($DestinationRow['PhaseID']!=$DestinationRow['Origin'])
				{ 
				
				
				$newupi=$DestinationRow['PBS']."/".UPI2TOK($UPI)['VARIANT']."/".$_COOKIE['Ilocation'].".".UPI2TOK($UPI)['SERIAL'];
				
				//print_r($DestinationRow);
				//return;
				ChangeUPI($UPI,$newupi);
				$conn=OpenWriteConn();
				$sql="UPDATE ASSIGNED_DOM SET PhaseID='".$DestinationRow['PhaseID']."' WHERE UPI='".$newupi."';";
				$res=$conn->query($sql);
				if(!$res) sqlog("NON FA UPDATE ASSIGNED_DOM IN EVOLVE : ".$sql,$conn->error);
				$conn->close();
				//print_r("{km3action: 'integration',DOMUPI: '".$newupi."', PhaseID: '".$DestinationRow['PhaseID']."',EV: '1'}");
				return "{km3action: 'integration',DOMUPI: '".$newupi."', PhaseID: '".$DestinationRow['PhaseID']."',EV: '1'}";
				
				} else {
					return "";
				}
				return;
				
				}
}
function ToStorage($StorID,$UPI)
{
	/*
	BUG FOUND ALSO PARTS MUST BE UPDATED FOR BATCH COMPONENTS 
	B_id Has NO SENSE FOR BATCH !
	*/
				
				$con=OpenWriteConn();
				
				
				/*
				$sql="UPDATE Parts a INNER JOIN DOM_INTEGRATION b on a.PARTUPI=b.PRODUPI SET a.B_id='".$StorID."',b.B_id='".$StorID."' where b.DOMUPI='".$UPI."';";
				$result = $con->query($sql);
				*/
				$sql="UPDATE ASSIGNED_DOM SET B_id='".$StorID."' where UPI='".$UPI."';";
				$result = $con->query($sql);
				//$sql="UPDATE DOM_INTEGRATION SET B_id='".$StorID."' where DOMUPI='".$UPI."';";
				//$result = $con->query($sql);
				$con->close();
				
}
function DelPart()
	{
		if (func_num_args() != 0) {
				$args = func_get_args();
				$UPI=$args[0];
				
				$UPI_tok=UPI2TOK($UPI);
				$conn=OpenWriteConn();
				$sql="DELETE FROM Parts WHERE   PBS='".$UPI_tok["PBS"]."' AND Variant='".$UPI_tok["VARIANT"].
				"' AND Version='".$UPI_tok["VERSION"]."' AND Serial='".$UPI_tok["SERIAL"]."';";
				
				$result = $conn->query($sql);
				
				$conn->close();
					 $message=$UPI." Deleted.";
					 if ($result)
					 ShowMessage($message);
				
		}
	}

function uniqidReal($lenght = 13) {
    // uniqid gives 13 chars, but you could adjust it to your needs.
    if (function_exists("random_bytes")) {
        $bytes = random_bytes(ceil($lenght / 2));
    } elseif (function_exists("openssl_random_pseudo_bytes")) {
        $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
    } else {
        throw new Exception("no cryptographically secure random function available");
    }
    return substr(bin2hex($bytes), 0, $lenght);
}
function GenUniqUPI($PBS,$serial=0)
	{
	return $PBS."/".uniqidReal(8)."/".$_COOKIE["Ilocation"].".".$serial;
	}
function IsBatch($PBS)
	{
		$conn=OpenReadConn();
		$sql = "SELECT ISBATCH from PBS where PBS='".$PBS."';";
		$result = $conn->query($sql);
		$conn->close();
		if ($result->num_rows ==1 ){
			$act = $result->fetch_assoc();
			return $act['ISBATCH'];
		}
	}
function WriteLogbook($Subject,$Message,$Produpi,$Position,$Type='INTEGRATION',$Fname="")
	{   

		if ($Fname!=""){
		$Fname=$Fname.".jpg";
		$uploaddir = ULOAD.$Fname;	
		$userfile_tmp = ULOAD_TMP.$Fname;
		
		if (rename($userfile_tmp, $uploaddir)) {
		}else{
        		
		$Fname="";
		}}
		Del_all_old(1);
		if (isset($_COOKIE['Ilocation'])&&isset($_COOKIE['km3user'])) 
		{ // Il ilocation and user are not set nobody is logged
		$Ilocation=$_COOKIE['Ilocation'];
		$User=$_COOKIE['km3user'];
		
		if (isset($_COOKIE['DOMUPI'])) $DOMUPI=$_COOKIE['DOMUPI'];
		else $DOMUPI='';
		
		if (isset($_COOKIE['km3bench'])) $Bench=$_COOKIE['km3bench'];
		else $Bench=$Ilocation; //Default storage if Bench not set
		
		
		$conn=OpenWriteConn();
		$sql = "INSERT INTO IntLog (DOMUPI,PRODUPI,POSITION,Site,KM3db_OID,B_id,Subject,Message,NoteType,Image_path)
		VALUES ('".$DOMUPI."','".$Produpi."','".$Position."','".$Ilocation."','".$User."','".$Bench."','".$Subject."','".$Message."','".$Type."','".$Fname."');";
		$result = $conn->query($sql);
		
		//print_r($sql);
		if (!$result) return sqlog($sql,$conn->error);
		$conn->close();
			}
		
		}
function Del_all_old ($h)
{/* TEST
  $files = glob(ULOAD_TMP."*");
  $now   = time();

  foreach ($files as $file) {
    if (is_file($file)) {
      if ($now - filemtime($file) >= 60 * 60 * $h) { // h hours
        unlink($file);
      }
    }
  }
*/}
function genbutton($name,$value,$OnlyButton=0,$action="")
{   //DEPRECATED 
	if ($action=="") $action=$_SERVER["PHP_SELF"];
	if ($OnlyButton)
	{
		echo '<input type="submit" name="'.$name.'" value="'.$value.'" formmethod="POST" formaction="'.htmlspecialchars($action).'" > </input>';
	}
	else
	{
	echo '<FORM><input type="submit" name="'.$name.'" value="'.$value.'" formmethod="POST" formaction="'.htmlspecialchars($action).'" > </input> </form> ';
	}
}
function QueryPartData($UPI,$DataName,$novar=FALSE)
{
	$UPI=rtrim($UPI,'.');
	$con=OpenReadConn();
	if($novar)
	{
		$sql="";
	}
	else
	{
		$sql="SELECT * FROM Part_data where PRODUPI='".$UPI."' AND DataName='".$DataName."'";
	}
	$result=$con->query($sql);
	$con->close();
	if ($result->num_rows==1)
	{
		return $result->fetch_assoc()["Datum"];
	} 
	else
	{
		return false;
	}
}
/*
function get_pmtcfg($UPI_Tok)
{
	
	//READS THE CFG FROM CENTRAL db NO MORE ! This is done offline by a CRON job ! ( at least for the moment )
	
	$data = array(
    'usr' => 'Glevi',
    'pwd' => 'xxxxxxxxxxxx',
    'label' => 'VENDOR',
	'pbs'   => $UPI_Tok['PBS'],
	'serialnumber' => $UPI_Tok['SERIAL'],
	'variant'=>$UPI_Tok['VARIANT']
	);
// https://km3netdbweb.in2p3.fr/xmlds/cfggroup/s?usr=Glevi&pwd=xxxxxxxxxxxx&label=VENDOR&pbs=3.4.2.3&serialnumber=1013&variant=PSEUDO
$arrContextOptions=array(
		'timeout' => 1200,
        "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ));  

$URL='https://km3netdbweb.in2p3.fr/xmlds/cfggroup/s?'.http_build_query($data);
$m = new SimpleXMLElement(file_get_contents($URL, false, stream_context_create($arrContextOptions)));
//$m->Data->ConfigurationGroup->Optics->InputParameters->InputParameterAliasValue->Value=999; Values can be changed if needed
$hv="";
try {
error_reporting(0);
$hv=$m->Data->ConfigurationGroup->Optics->InputParameters->InputParameterAliasValue->Value;
error_reporting(1);
} catch (Exception $e)
{
	
}
return $hv;
}
*/
function cf_build($UPI,$HV=-1,$THR=-1)// Add Thr and HV ; THR is ignored in this first version
{
/*
PMT Configuration group build V0.2
*/
$m=simplexml_load_file(RSDIR.DEFPMT_THRHVCFG);

$UPI_Tok = UPI2TOK($UPI);
$hv=QueryPartData($UPI,"PMT_HIGHVOLT");
if ($HV>=0) $hv=$HV;
if ($HV<0) $HV=100;
$thr=QueryPartData($UPI,"PMT_THRESHOLD");
if ($THR>=0) $thr=$THR;
if ($THR<0) $THR=47; //47 is the default
if ($hv==FALSE) $hv=$HV;
if ($thr==FALSE) $thr=$THR;
//print_r($UPI_Tok);
$m->Data->ConfigurationGroup->OID=uniqidReal();
$m->Data->ConfigurationGroup->Filter->Variant=$UPI_Tok['VARIANT'];
$m->Data->ConfigurationGroup->Filter->Version=$UPI_Tok['VERSION'];
$m->Data->ConfigurationGroup->Filter->SerialNumber=$UPI_Tok['SERIAL'];
$m->Data->ConfigurationGroup->Optics->InputParameters->InputParameterAliasValue[0]->Value=$hv;
$m->Data->ConfigurationGroup->Optics->InputParameters->InputParameterAliasValue[1]->Value=$thr;
//print_r($m);
/*
	Costum CFG
	https://km3netdbweb.in2p3.fr/xmlds/cfggroup/s?label=VENDOR&pbs=3.4.2.3&serialnumber=1000&@label=mine&@p[PMT_HIGHVOLT]=100&@p[PMT_THRESHOLD]=126&usr=Glevi&pwd=xxxxxxxxxxxx
//$m = new SimpleXMLElement(file_get_contents($URL, false, stream_context_create($arrContextOptions)));
*/


$cfg=$m->Data->ConfigurationGroup;
$doc = new DOMDocument();
$doc->formatOutput = TRUE;
$doc->loadXML($cfg->asXML());   //////////////// ERROR 500 !
//$doc->loadXML($cfg);
$xml = $doc->saveXML();
$xml = explode('<?xml version="1.0"?>', $xml)[1];

return $xml;
}


function rs_build($domupi="",$hfile="",$ffile="")
{ /*
Build runsetup file
*/  $IDD=GenRSoid($domupi);
	
	if (isset($argv[1])) $hfile=$argv[1];
		
		$header=file_get_contents($hfile);
		$header=str_replace("RS_NAME","KM3DIA_DAQ_CALIBRATION_RUN_",$header);
		
		$header=str_replace("RS_DETECTOR",$domupi,$header);
		$header=str_replace("RS_DESCRIPTION","KM3DIA generated RS for: ".$domupi,$header);
	
	if (isset($argv[2])) $ffile=$argv[2];
			$footer=file_get_contents($ffile);
	
	$con=OpenReadConn();
	$sql="SELECT * from DOM_INTEGRATION WHERE DOMUPI='".$domupi."' and PRODUPI LIKE '3.4.2.3%'";
	$result=$con->query($sql);
	
	$CFGs=""; // string that will contain all Configuration Groups from
	if ($result->num_rows >0 ) // if DOM exist and there are at least 1 PMT
		{
		while ($row = $result->fetch_assoc())
			{// For every PMT ask for CFG
	//		print_r($row['PRODUPI']);
			$CFGs=$CFGs.cf_build($row['PRODUPI']); // Read CFG FROM DB This function is
			
			}	
		}
	$con->close();
	

	$all=$header.$CFGs.$footer;
	
	return $all;
}
	
/*
TODO MV JPG FILE FROM TMP TO STORAGE
*/


function GenXML($UPIin,$Ilocation="")
{
/*
XML GENERATION FOR DAQ AND KM3DB V0.5
DONE CORRECT FOR COUPLED COMPONENTS
*/
$UPIs=array();

if (is_array($UPIin))
	{
      $UPIs=$UPIin;
    } else {
      $UPIs[0]=$UPIin;
    }
		if ($Ilocation=="")
		$Ilocation=$_COOKIE['km3location']; 
		$User=$_COOKIE['km3user'];
		$date=date('Y-m-d');
		
	    $XMLOUT="<IntegrationInfo><ResponsibleUser>".$User."</ResponsibleUser><Location>".$Ilocation."</Location><Start>".$date."</Start><End>".$date."</End><Notes>KM3DIA GENERATED</Notes><Status>COMPLETED</Status><Decision>ACCEPT</Decision><Containers>";
		$conn=OpenReadConn();
	    foreach ($UPIs as $UPI){
		
		$sql = "select * from DOM_INTEGRATION INNER JOIN  DOMPOSPBS ON DOMPOSPBS.POS=DOM_INTEGRATION.POS where DOM_INTEGRATION.DOMUPI='".$UPI."' AND DOMPOSPBS.SUBPART='".UPI2TOK($UPI)['PBS']."';";
		$result = $conn->query($sql);
		if ($result) {
			$XMLOUT=$XMLOUT."<ProductIntegrationInfo>   <UPI>".$UPI."</UPI>   <Notes></Notes>   <Contents>";
			while($row = $result->fetch_assoc()) {
			//$row['Id']=(int)$row['Id']-1;
			preg_match_all('!\(\d+\)!', $row['POS'], $m);
			preg_match_all('!\d+!', $m[0][0], $matches);
			$row['Id']=ltrim($matches[0][0], "0");
			if ($row['Id']=="") $row['Id']="0";
			
			$XMLOUT=$XMLOUT."<ContentIntegrationInfo><Position>".$row['Id']."</Position><UPI>".$row['PRODUPI']."</UPI></ContentIntegrationInfo>";
			
		}} else {sqlog($sql,$conn->error);}
		$Coupld=QueryCoupled($UPI);
		if ($Coupld)
			if ($Coupld->num_rows>0)
				while($Coupls = $Coupld->fetch_assoc()) {
					$sql = "select * from DOM_INTEGRATION INNER JOIN  DOMPOSPBS ON DOMPOSPBS.POS=DOM_INTEGRATION.POS where DOM_INTEGRATION.DOMUPI='".$Coupls['UPI']."' AND DOMPOSPBS.SUBPART='".UPI2TOK($Coupls['UPI'])['PBS']."';";
					$result = $conn->query($sql);
					if ($result) {
						while($row = $result->fetch_assoc()) {
						//row['Id']=(int)$row['Id']-1;
						preg_match_all('!\(\d+\)!', $row['POS'], $m);
						preg_match_all('!\d+!', $m[0][0], $matches);
						$row['Id']=ltrim($matches[0][0], "0");
						if ($row['Id']=="") $row['Id']="0";
						$XMLOUT=$XMLOUT."<ContentIntegrationInfo><Position>".$row['Id']."</Position><UPI>".$row['PRODUPI']."</UPI></ContentIntegrationInfo>";
			
						}
					} else {sqlog($sql,$conn->error);}
				}
		$XMLOUT=$XMLOUT."</Contents>  </ProductIntegrationInfo>";
		}
		$XMLOUT=$XMLOUT."</Containers></IntegrationInfo>";
		$conn->close();
		return trim(preg_replace('/\s\s+/', '  ', htmlspecialchars($XMLOUT)));
		 
}
function GenOPT($UPI)
{/*
OPT GENERATION FOR DAQ AND KM3DB V0.2
DONE CORRECT FOR COUPLED COMPONENTS: NOT VERIFIED BETA
#Optics configuration storage
#24/11/17 12:11:49
ch_enabled=7fffffff
ch0.id=2162
ch0.hv=102
ch0.ths=47
*/
		$Ilocation=$_COOKIE['Ilocation'];
		$User=$_COOKIE['km3user'];
	    $OPTOUT="#Optics configuration storage\\n#Generated by:".$User."\\n#At Location".$Ilocation."\\n#".date('d/m/y h:m:s')."\\n#DOM ".$UPI."\\n";
		$conn=OpenReadConn();
		$sql = "select DISTINCT * from DOM_INTEGRATION INNER JOIN  DOMPOSPBS ON DOMPOSPBS.POS=DOM_INTEGRATION.POS where DOM_INTEGRATION.DOMUPI='".$UPI."' AND DOMPOSPBS.SUBPART='".UPI2TOK($UPI)['PBS']."';";
		$result = $conn->query($sql);
		if ($result) {
			while($row = $result->fetch_assoc()) {
			if(UPI2TOK($row['PRODUPI'])['PBS']=='3.4.2.3'){
				$row['Id']=(int)$row['Id']-1;
				$OPTOUT=$OPTOUT."ch".$row['Id'].".id=".QueryPartData($row['PRODUPI'],'PMT_PROMISID')."\\n";
				$OPTOUT=$OPTOUT."ch".$row['Id'].".hv=".QueryPartData($row['PRODUPI'],'PMT_HIGHVOLT')."\\n";
				$OPTOUT=$OPTOUT."ch".$row['Id'].".ths=47\\n";
				}
			}
		} else {sqlog($sql,$conn->error);}
		$Coupld=QueryCoupled($UPI);
		if ($Coupld)
			if ($Coupld->num_rows>0)
				while($Coupls = $Coupld->fetch_assoc()) {
					$sql = "select DISTINCT * from DOM_INTEGRATION INNER JOIN  DOMPOSPBS ON DOMPOSPBS.POS=DOM_INTEGRATION.POS where DOM_INTEGRATION.DOMUPI='".$Coupls['UPI']."' AND DOMPOSPBS.SUBPART='".UPI2TOK($Coupls['UPI'])['PBS']."';";
					$result = $conn->query($sql);
					$OPTOUT=$OPTOUT."#coupled ".$Coupls['UPI']."\\n";
					if ($result) {
						while($row = $result->fetch_assoc()) {
						if(UPI2TOK($row['PRODUPI'])['PBS']=='3.4.2.3'){
							$row['Id']=(int)$row['Id']-1;
							$OPTOUT=$OPTOUT."ch".$row['Id'].".id=".QueryPartData($row['PRODUPI'],'PMT_PROMISID')."\\n";
							$OPTOUT=$OPTOUT."ch".$row['Id'].".hv=".QueryPartData($row['PRODUPI'],'PMT_HIGHVOLT')."\\n";
							$OPTOUT=$OPTOUT."ch".$row['Id'].".ths=47\\n";
							}
						
						}
					} else {sqlog($sql,$conn->error);}
				}
		$conn->close();
		$OPTOUT=$OPTOUT."ch_enabled=0";
		return $OPTOUT;
		//return trim(preg_replace('/\s\s+/', '  ', htmlspecialchars($XMLOUT)));
	
}

function pingAddress($ip) {
	/* UNIX VERSION !*/
    exec("/bin/ping -c 1 -w 1 $ip", $outcome, $status); // 1 sec timeout
    if (0 == $status) {
        return 1; //so that an if (pingAdrress(....)) will be true
    } else {
        return 0;
    }
    
}
function ClearExpiredTokens()
{
	$con=OpenWriteConn();
	$sql = "UPDATE Operators  set SSkey=NULL,Expire=NULL where Expire < ".time().";";
	$result = $con->query($sql);
	$con->close();	
}
function CheckWriteToken($key,$user)
{
	ClearExpiredTokens();
	
	$con=OpenReadConn();
	$sql = "SELECT * FROM Operators where KM3db_OID = '".$user."';";
	$result = $con->query($sql);
	if(!$result) sqlog($sql,$con->error);
	print_r($result);
	
	if ($result->num_rows == 1  ) { // Only One person Logon
		$Urow = $result->fetch_assoc();
		print_r($Urow);
		if (password_verify($key, $Urow['SSkey'])) {
	 print_r("Passed");
		$con -> close();
		return true;
	} else 
	{
		$con -> close();
		print_r("Not Passed");
		return false;
	}
	}
	else 
	{   $con -> close();
		print_r("No Row");
		return false;
	}
}

function ipcheck($ip)
{
	
	$conn=OpenWriteConn();
	
	$sql="select * from netinfo where Ipadd = INET_ATON('".$ip."'); ";
	
	$result=$conn->query($sql);
	if  (!($result)) sqlog($sql,$conn->error) ;
	$url="https://ipapi.co/".$ip."/json/?key=n7fowb8cjnAuDy6xYACJTImDiZUaSsUBPEpODbrBGsNhS5hgv5";
	
	if ($result)
	if($result->num_rows==0)
	{

$curlSES=curl_init(); 

curl_setopt($curlSES,CURLOPT_URL,$url);
curl_setopt($curlSES,CURLOPT_RETURNTRANSFER,true);
curl_setopt($curlSES,CURLOPT_HEADER, false); 

$jrec=curl_exec($curlSES);

curl_close($curlSES);
//step5
	
	$sql="insert into netinfo (Ipadd,INFO) values( INET_ATON('".$ip."'),'".$jrec."'); ";
	
	$result=$conn->query($sql);
	if  (!($result)) sqlog($sql,$conn->error) ;
	//sqlog($sql,"----INSERT netinfo----") ;
	$conn->close();
	return  $jrec;
	
	} else
	{
		$conn->close();
		//sqlog($sql,"----SELECT netinfo----") ;
		return $result->fetch_assoc()['INFO'];
	}
		
	
	
	
}
/* function CheckRegex($Value,$ETid,$Mid)
{
	
	//V0.1 To test Check if value match the Regex stored for 
	
	
	$conn=OpenReadConn();
	$pattern='^$'; //Empty string Regex
	//Retrive Regex
	$sql="select Regex from Measure_Type as mt left join  Test_Measures as tm on tm.M_T_id=mt.M_T_id left join Test as Te on tm.T_id=Te.T_id left join Measure as me on me.E_T_id=Te.E_T_id where Te.E_T_id='".$ETid."' and me.M_id='".$Mid."';";
	$result=$conn->query($sql);
	if ($result)
	{
		$pattern=$result->fetch_assoc()['Regex'];
		}
		else {sqlog($sql,$conn->error);}
	//var_dump($pattern);
	$conn->close();
	return (preg_match($pattern, $Value));
} 
function Jpparse($json,$test)
{

	Inserted in INTEGRATION-PHP
{
"module":816928406,
"gains":[0.959,1.104,1.145,0.850,1.079,1.201,1.086,1.093,1.160,1.264,1.192,1.221,1.393,0.924,1.024,1.239,1.236,0.898,1.217,1.146,1.024,1.055,1.025,1.387,1.194,1.152,1.286,1.209,1.115,1.169,1.297],
"rates":[0.722,0.997,0.806,0.531,0.544,1.263,0.781,1.380,0.580,0.738,1.216,1.136,0.323,0.364,0.241,0.389,0.386,0.393,0.493,0.204,0.399,0.265,0.291,0.202,0.234,0.187,0.354,0.459,0.281,0.187,0.311]
}

	
	$conn=OpenReadConn();
	
	$conn->close();
}*/
	?>