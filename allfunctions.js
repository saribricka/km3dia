
function Sxml(Xml)
{   var Xmo=(new DOMParser()).parseFromString(Xml, "text/xml")
	
var blob = new Blob(['Hello, world!'], {type: 'text/plain;charset=utf-8'});
FileSaver.saveAs(blob, 'hello_giuseppe.txt');
console.log("Hello world!");
}

function PopupCenter(title)
{
  var w = 400;
  var h = 250;
  var l = Math.floor((screen.width-w)/2);
  var t = Math.floor((screen.height-h)/2);
  window.open("testwcam.php?Tit="+title+"","","width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
}
function setCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function getCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

function on() {
    document.getElementById("overlay").style.display = "block";
	Webcam.set({
		width: 320,
		height: 240
	});
	
		Webcam.attach( "#my_camera" );
}
function onnote() {
    document.getElementById("Notepad").style.display = "block";
	Webcam.set({
		width: 640,
		height: 480
	});
	
		Webcam.attach( "#my_camera_2" );
}
function off() {
    document.getElementById("overlay").style.display = "none";
	Webcam.reset();
}
function take_snapshot(Fname) {
			Webcam.snap( function(data_uri) {
				console.log(Fname);
			document.getElementById("my_result").innerHTML = "<img src='"+data_uri+"'></img>";
			Webcam.upload( data_uri, 'uplooad.php?Tit='+Fname, function(code, text) {} );
			
			} );
		}
function generateRandomString(l) {
    var chars = '0123456789abcdefghijklmnopqrstuvwxyz!$?&%_ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var charsLength = chars.length;
    var string = '';
	for(var i=0; i<l; i++)
  		string += chars.charAt(Math.floor(Math.random() * charsLength));
	string+='.';
  	return string;
}
function openTab(evt, TabName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("tabcontent");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
	document.getElementById(TabName).style.display = "block";
	document.getElementById("B"+TabName).className += " w3-red";
	document.getElementById("snapForm").value = TabName ; 

  //evt.currentTarget.className += " w3-red";
}


function utter($phrase,$lang){
		responsiveVoice.speak($phrase,$lang);
		
		}
//0 padding function for date printing
Number.prototype.pad = function(size) {
  var s = String(this);
  while (s.length < (size || 2)) {s = "0" + s;}
  return s;
}
function clearBox(elementID)
{
    document.getElementById(elementID).innerHTML = "";
}
function ShowQRCode($String,$message)
{	
	clearBox('qrcode');
	new QRCode(document.getElementById('qrcode'),$String);
	document.getElementById("message").innerText =$message;

}
//Write function for date needs an element named "date"
  function writetime(){
    var now = new Date();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
	var timestring= (hour).pad(2)+":"+(minute).pad(2)+":"+(second).pad(2);
	var element = document.getElementById("date");
	document.getElementById("date").style.textAlign= "right";
	element.innerText = timestring;
	
    }
	ElId="QRC"; // Global variable for QR code recognition.
function startscanner() {
	if (scanner_on==0) 
	{
		scanner_on=1;
		ElId="QRC";
		if (arguments.length>0){ElId=arguments[0]} 
		console.log(ElId);
      scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
	  
      scanner.addListener('scan', function (content) {
        console.log(content);
		console.log(ElId);
		
      document.getElementById(ElId).value = content.replace("https://km3netdbweb.in2p3.fr/product/","");
	//
	//
	
      });
      Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
          scanner.start(cameras[0]);
		  document.getElementById("preview").style.visibility="visible";
        } else {
		 
          console.error('No cameras found.');
        }
      }).catch(function (e) {
        console.error(e);
      });
	  } else {
		  scanner_on=0;
		  scanner.stop();
		  delete scanner;
		  document.getElementById("preview").style.visibility="hidden";
	  }
}
function post(path, params, method) {
	// From: https://stackoverflow.com/questions/133925/javascript-post-request-like-a-form-submit
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
	// post('/contact/', {name: 'Johnny Bravo'});
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

 function loadImage(url, altUrl) {
    var timer;
	var rr;
	rr=url;
    function clearTimer() {
        if (timer) {                
            clearTimeout(timer);
            timer = null;
        }
    }

    function handleFail() {
        // kill previous error handlers
        this.onload = this.onabort = this.onerror = function() {};
        // stop existing timer
        clearTimer();
        // switch to alternate url
		//document.write(rr); 
        if (this.src === url) {
            this.src = altUrl;
			
        }
		rr=altUrl;
		console.log('xxxx');
		console.log(rr); 
		setCookie("CU_CONNECT", "0", "1");
    }

    //var img = new Image();
	var img = document.getElementById("JJimg");
    img.onerror = img.onabort = handleFail;
    img.onload = function() {
        clearTimer();
		console.log('yyyy');

		setCookie("CU_CONNECT", "1", "1");
    };
    img.src = url;
	
    timer = setTimeout(function(theImg) { 
        return function() {
            handleFail.call(theImg);
        };
    }(img), 3000);
	
	console.log(img.src);
	console.log(rr);
    return(img.src);
}

