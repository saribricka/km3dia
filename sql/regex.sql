USE KM3DIA
ALTER TABLE Measure_Type ADD COLUMN Regex VARCHAR(300) DEFAULT '/.*/'; 
SET @RGDEFLT := '/.*/';
SET @RGFLOAT := '/^\\s*[+-]?\\d*(\\.\\d*)?(E[+-]?\\d+)?\\s*$/';
SET @RGYESNO := '/^\\s*((?i)Y|YES|n|NO)\\s*$/';
SET @RGMACAD := '/(([0-9A-Fa-f]{2}[-:]){5}[0-9A-Fa-f]{2})|(([0-9A-Fa-f]{4}\\.){2}[0-9A-Fa-f]{4})/';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '1';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '2';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '3';

UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '5'; 
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '6';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '7';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '8';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '9';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '10';
UPDATE Measure_Type SET Regex=@RGYESNO where M_T_id= '11'; -- No Unit Yes or No... in any case
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '16';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '17';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '18';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '19';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '20';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '21';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '22';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '23';
UPDATE Measure_Type SET Regex=@RGFLOAT where M_T_id= '24';
UPDATE Measure_Type SET Regex=@RGMACAD where M_T_id= '25'; -- Mac ADDRESS