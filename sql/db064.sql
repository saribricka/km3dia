#
# KM3DIA DATABASE Version 0.64
#
# Multi "TOWN" Integration Sites
/*
Multi SubPart.
*/
DROP DATABASE KM3DIA;
CREATE DATABASE KM3DIA
DEFAULT CHARACTER SET utf8 
DEFAULT COLLATE utf8_general_ci;
USE KM3DIA;
DROP USER IF EXISTS  km3user;
CREATE USER  km3user  IDENTIFIED BY  'pyrosoma' ;
GRANT SELECT ON KM3DIA.* TO  km3user ;
DROP USER IF EXISTS  km3writer;
CREATE USER  km3writer  IDENTIFIED BY  'PyrosomaKM3NET' ;
GRANT SELECT ON KM3DIA.* TO  km3writer ;
GRANT INSERT ON KM3DIA.* TO  km3writer ;
GRANT UPDATE ON KM3DIA.* TO  km3writer ;
GRANT DELETE ON KM3DIA.* TO km3writer;
CREATE TABLE IF NOT EXISTS netinfo
(
Ipadd BIGINT,
INFO VARCHAR(3000),
TimeS TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY ( Ipadd )
);


CREATE TABLE IF NOT EXISTS SwVersion
(
     SwName VARCHAR(300),
	 SwVersion VARCHAR(300),
	 SwRelNotes VARCHAR(3000),
	 PRIMARY KEY ( SwName )
);
INSERT INTO SwVersion VALUES('KM3DIA','0.9','First Public DOM INTEGRATION');

CREATE TABLE IF NOT EXISTS ISites (
        SiteID  INT NOT NULL,
		Town	VARCHAR(30),
		PRIMARY KEY (SiteID)
);

CREATE TABLE IF NOT EXISTS Site  (
		KM3db_LOC_ID  VARCHAR(30) NOT NULL,
		Town_Details  VARCHAR(300),
		SiteID        INT NULL,
		FOREIGN KEY (SiteID) REFERENCES ISites(SiteID),
		PRIMARY KEY ( KM3db_LOC_ID )
);
CREATE TABLE IF NOT EXISTS Possible_Settings  (
		SettingName VARCHAR(30),
		Description VARCHAR(300),
		PRIMARY KEY (SettingName)
);
INSERT INTO Possible_Settings VALUES('CU_ADDR','IP ADDRESS OF CU');
INSERT INTO Possible_Settings VALUES('km3DIALA_IP','IP ADDRESS OF LKM3DIA');
INSERT INTO Possible_Settings VALUES('DMADDR','IP ADDRESS OF DM');
INSERT INTO Possible_Settings VALUES('STRING_N','STRING NUMBER');
INSERT INTO Possible_Settings VALUES('DOM_N','DOM NUMBER');
INSERT INTO Possible_Settings VALUES('DOM_IP','DOM IP');
/* put also km3dia ip ?*/
CREATE TABLE IF NOT EXISTS Site_Settings  (
/* 
Parameters related to each site as:
Control Unit Address___ Table to be used in DAQ interaction
or L_KM3DIA ip Addsess,
SiteId=NULL is intended for global settings valid in every site.
*/
		SetID         INT AUTO_INCREMENT,
        SiteID        INT,
		SettingName VARCHAR(30),
		SettingValue VARCHAR(300),
		FOREIGN KEY (SiteID) REFERENCES ISites(SiteID),
		FOREIGN KEY (SettingName) REFERENCES Possible_Settings(SettingName),
		PRIMARY KEY ( SetID )
);
CREATE TABLE IF NOT EXISTS DAQ_DATA  (
/*
The RAW data acquired from CU or other service
*/
		DOMUPI VARCHAR(300),
		DAQName VARCHAR(30),
		DATA_TS	TIMESTAMP,
		Datum MEDIUMTEXT,
		PRIMARY KEY(DOMUPI,DAQName,DATA_TS)
/*		FOREIGN KEY (DOMUPI) REFERENCES ASSIGNED_DOM(UPI), // THIS KEY is defined after  ASSIGNED DOM 
	FOREIGN KEY (DAQName) REFERENCES DAQ_Request(DAQName)*/	
);

CREATE TABLE IF NOT EXISTS Bench  (
	 B_id  int NOT NULL AUTO_INCREMENT,
	 B_Details  VARCHAR(300),
	 SiteID  INT NOT NULL,
	 B_type BOOLEAN NOT NULL, -- Work bench=0 Storage=1
	 FOREIGN KEY (SiteID) REFERENCES ISites(SiteID),
	 PRIMARY KEY ( B_id )
	 
);



CREATE TABLE IF NOT EXISTS PBS
(
	PBS VARCHAR(30),
	VARIANT VARCHAR(30),
	Description VARCHAR(300),
	ISBATCH BOOLEAN,
	PRIMARY KEY ( PBS, VARIANT )
);
/*
Id could be eliminated from DOMPOSPBS table ( just the number of Position )
*/
CREATE TABLE IF NOT EXISTS DOMPOSPBS
(   Id  INT,
	POS VARCHAR(30),
	PBS VARCHAR(30),
	VARIANT VARCHAR(30),
	SUBPART VARCHAR(30),
	FOREIGN KEY ( PBS,VARIANT ) REFERENCES  `PBS` ( PBS,VARIANT ),
	PRIMARY KEY ( POS,SUBPART )
);

CREATE TABLE IF NOT EXISTS `IntegrationPhase`
(
	PhaseID INT,
	PhaseName VARCHAR(60),
	Origin	INT,
	Destination INT,
	PBS VARCHAR(30),
	PRIMARY KEY (PhaseID)

);

CREATE TABLE `ASSIGNED_DOM` 
(
/* Contains information about:
1) DOM assigned for integration to the Site 
2) SUBPART Integrated 
Only DOM have KM3db_LOC_ID NOT NULL
*/
	`UPI` VARCHAR(300),
	`KM3db_LOC_ID` VARCHAR(30) NULL,
	SiteID        INT NULL,
	PhaseID INT NULL,		-- Integration Phase
	`B_id` int,
	Complete BOOLEAN NOT NULL DEFAULT 0,
	NotConform BOOLEAN NOT NULL DEFAULT 0,
	PBS varchar(30) AS (LEFT( UPI, LOCATE("/",UPI) - 1)),
	FOREIGN KEY (SiteID) REFERENCES ISites(SiteID),
	FOREIGN KEY ( KM3db_LOC_ID ) REFERENCES  Site ( KM3db_LOC_ID ),
	FOREIGN KEY ( B_id ) REFERENCES  Bench ( B_id ),
	PRIMARY KEY (`UPI`),
	FOREIGN KEY ( PhaseID ) REFERENCES  `IntegrationPhase` ( PhaseID )
);
GRANT DELETE ON KM3DIA.ASSIGNED_DOM TO km3writer;
ALTER TABLE  `DAQ_DATA`  ADD CONSTRAINT  DaqD_fk1  FOREIGN KEY ( DOMUPI ) REFERENCES ASSIGNED_DOM(UPI);

CREATE TABLE `DOM_INTEGRATION` (
/* Contains information about:
DOM parts and DOMS integrated at site

*/
  DOMUPI VARCHAR(60), 	-- Identify subpart OR DOM
  POS VARCHAR(30),    	-- Position in it
  PRODUPI VARCHAR(60), 	-- Part integrated
  `B_id` int,
  Modifiable BOOLEAN NOT NULL DEFAULT 1,
  PRIMARY KEY (DOMUPI,POS),
  FOREIGN KEY ( DOMUPI ) REFERENCES  `ASSIGNED_DOM` ( UPI ),
  FOREIGN KEY ( POS ) REFERENCES  `DOMPOSPBS` ( POS ),
  FOREIGN KEY ( B_id ) REFERENCES  Bench ( B_id )
  
);
GRANT DELETE ON KM3DIA.DOM_INTEGRATION TO km3writer;



CREATE TABLE IF NOT EXISTS Operators
(    Skey    VARCHAR(300),
	 KM3db_OID  VARCHAR(30),
	 KM3db_LOC_ID   VARCHAR(30),
	 Name  VARCHAR(300),
	 Surname  VARCHAR(300),
	 O_Level  int,
	 SSkey    VARCHAR(300),
	 Expire   INT,
	PRIMARY KEY ( KM3db_OID ),
	FOREIGN KEY ( KM3db_LOC_ID ) REFERENCES  Site ( KM3db_LOC_ID )
);



CREATE TABLE IF NOT EXISTS Parts  (
	 Pb_id   INT NOT NULL AUTO_INCREMENT,
	 PBS  VARCHAR(30),
	 Variant  VARCHAR(30),
	 Version  int,
	 Serial  int,
	 Position  VARCHAR(30),
	 B_id  INT,
	 IsPartof VARCHAR(60) ,
	 PARTUPI varchar(60) AS (concat(`PBS`,'/',`VARIANT`,'/',`Version`,'.',`Serial`)),
	PRIMARY KEY ( Pb_id ),
	FOREIGN KEY ( B_id ) REFERENCES  Bench ( B_id ),
	FOREIGN KEY ( IsPartof ) REFERENCES  `ASSIGNED_DOM` ( UPI ),
	FOREIGN KEY ( Pbs ) REFERENCES  PBS ( PBS )
);
CREATE TABLE IF NOT EXISTS Part_data  (
/*
Store calibration and vendor data for parts  needing it
is filled asyncronously from KM3DB during integration.
Is Read when a detector description is requested by DAQ
Version 0.1
*/
	PRODUPI VARCHAR(60),
	DataName VARCHAR(60),
	Datum VARCHAR(60),
	PRIMARY KEY (PRODUPI,DataName)
);
/*
CREATE TABLE IF NOT EXISTS LogTypes(
Type_id Int,
LType varchar(30),
Primary KEY (Type_id)
);
*/
CREATE TABLE IF NOT EXISTS IntLog  (
	OpId INT NOT NULL AUTO_INCREMENT,
	NoteType ENUM('USER', 'NCR','INTEGRATION','MEASURE') NOT NULL DEFAULT 'USER',
	Tims TIMESTAMP,
	DOMUPI VARCHAR(60),
	PRODUPI VARCHAR(60),
	Position VARCHAR(30),
	Site INT,
	KM3db_OID VARCHAR(30),
	B_id INT,
	Subject VARCHAR(50),
	Message VARCHAR(500),
	Image_path VARCHAR(50),
	Primary KEY (OpId)
);
GRANT DELETE ON KM3DIA.IntLog TO km3writer;


CREATE TABLE IF NOT EXISTS PossibleOperations  (
/*
*/
	 Pop_id  int NOT NULL AUTO_INCREMENT,
	 A_level  int,
	 Description  VARCHAR(3000),
	 Visible BOOLEAN,
	 PRIMARY KEY ( Pop_id )
);
/*
Note that in the runsetup tables the FK link is missing
*/
CREATE TABLE IF NOT EXISTS Runtype  (
Runtype   INT,
Hederfile varchar(30),
Footerfile varchar(30),
PMTfile   varchar(30),
Description varchar(300),
PRIMARY KEY ( Runtype )
);
INSERT INTO Runtype VALUES('1','h1.xml','f2.xml','default_pmt_cfg.xml','Default Runtype for TEST');
CREATE TABLE IF NOT EXISTS Runsetups  (
Fileid varchar(30),
Filehash varchar(30),
Runtype   INT DEFAULT '1',
FOREIGN KEY  (`Runtype`) REFERENCES `Runtype` (`Runtype`),
PRIMARY KEY (Fileid)
);

CREATE INDEX IDX_Bench on Parts (B_id); 
CREATE INDEX IDX_Site on Site (KM3db_LOC_ID); 
CREATE INDEX IDX_PBS on PBS (PBS);
CREATE INDEX IDX_IPH on IntegrationPhase(PhaseID);
/*Definition Of integration phases*/
INSERT INTO IntegrationPhase VALUES('1','TOP HEMISPHERE', NULL ,'2','3.4.5.1'); /*PRODUCTION LINE*/
/*INSERT INTO IntegrationPhase VALUES('1','TOP HEMISPHERE', NULL ,NULL,'3.4.5.1'); /*TEST LINE !*/
INSERT INTO IntegrationPhase VALUES('2','TOP HEMISPHERE HE-TESTED', '1' ,'5','3.4.5.1.1');
/*INSERT INTO IntegrationPhase VALUES('3','TOP HEMISPHERE AFTER SPLICE TEST', '2' ,'5','3.4.5.1.2');*/
INSERT INTO IntegrationPhase VALUES('4','TOP SUPPORT', NULL ,'5','3.4.5.2');
INSERT INTO IntegrationPhase VALUES('5','INSTRUMENTED TOP HEMISPHERE', '5' ,'10','3.4.5.1.2'); -- TWO Origins ?
INSERT INTO IntegrationPhase VALUES('6','BOTTOM HEMISPHERE', NULL ,'8','3.4.5.3');
INSERT INTO IntegrationPhase VALUES('7','BOTTOM SUPPORT', NULL ,'8','3.4.5.4');
INSERT INTO IntegrationPhase VALUES('8','INSTRUMENTED BOTTOM HEMISPHERE ASSEMBLY', '8' ,'-10','3.4.5.3.1'); 

INSERT INTO IntegrationPhase VALUES('10','FUNCTIONAL TEST UP', '5' ,'11','3.4.5.1.3');
INSERT INTO IntegrationPhase VALUES('-10','FUNCTIONAL TEST DOWN', '8' ,'-11','3.4.5.3.2');
INSERT INTO IntegrationPhase VALUES('11','Top Hemisphere Finalization', '10' ,'13','3.4.5.1.4');
INSERT INTO IntegrationPhase VALUES('-11','Bottom Hemisphere Finalization', '-10' ,'13','3.4.5.3.3');
INSERT INTO IntegrationPhase VALUES('13','Dom Closure', '13' ,'14','3.4.5.5.5');
INSERT INTO IntegrationPhase VALUES('14','Acceptance Test', '13' , '14' ,'3.4'); -- DOM DELIVERY

ALTER TABLE  `IntegrationPhase`  ADD CONSTRAINT  Iphase_fk0  FOREIGN KEY ( Origin ) REFERENCES  IntegrationPhase ( PhaseID );
ALTER TABLE  `IntegrationPhase`  ADD CONSTRAINT  Iphase_fk1  FOREIGN KEY ( Destination ) REFERENCES  IntegrationPhase ( PhaseID );
/* Test Must be introduced after phases because of dependancy*/


CREATE TABLE `Test_Type` (
  `T_id` INT,
  `T_name`  VARCHAR(30),
  `PhaseId` INT,
  `JSONBOX`   VARCHAR(30) DEFAULT "",
  FOREIGN KEY  (`PhaseId`) REFERENCES `IntegrationPhase` (`PhaseId`),
  PRIMARY KEY  (`T_id`)
);
INSERT INTO Test_Type(T_id,T_name,PhaseID) VALUES( '1','Helium Test','1');
INSERT INTO Test_Type(T_id,T_name,PhaseID) VALUES( '2','Splice Test','2');
INSERT INTO Test_Type(T_id,T_name,PhaseID) VALUES( '3','Functional Test','10');
INSERT INTO Test_Type(T_id,T_name,PhaseID) VALUES( '4','Acceptance Test General','14');
INSERT INTO Test_Type(T_id,T_name,PhaseID,JSONBOX) VALUES( '5','Acceptance Test Dark Current','14','rates');
INSERT INTO Test_Type(T_id,T_name,PhaseID,JSONBOX) VALUES( '6','Acceptance Test Time over Th','14','gains');

CREATE TABLE `Measure_Type` (
/*Definition of each measure type*/
  `M_T_id` INT,
  `Unit` VARCHAR(30),
  `MMin` VARCHAR(30),
  `MMax` VARCHAR(30),
  `Dval` VARCHAR(30) DEFAULT '',
  `Regex` VARCHAR(300) DEFAULT '/.*/',
  PRIMARY KEY  (`M_T_id`)
);
SET @RGDEFLT := '/.*/';
SET @RGFLOAT := '/^\\s*[+-]?\\d*(\\.\\d*)?(E[+-]?\\d+)?\\s*$/';
SET @RGYESNO := '/^\\s*((?i)Y|YES|n|NO)\\s*$/';
SET @RGMACAD := '/(([0-9A-Fa-f]{2}[-:]){5}[0-9A-Fa-f]{2})|(([0-9A-Fa-f]{4}\\.){2}[0-9A-Fa-f]{4})/';
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '1','mbar l/s','0','0.1',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '2','dBm',' ',' ',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '3','dBm',' ','0.2',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax) VALUES( '4','No Unit',' ',' '); -- No Unit Generic Not used
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '5','A','0.4','0.5',@RGFLOAT); 
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '6','s',' ','90',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '7','&deg;C',' ','40',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '8','&deg;C',' ','35',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '9','dB','-130','-100',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '10','dBm','0','5',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,Regex) VALUES( '11','No Unit', @RGYESNO); -- No Unit Yes or No... in any case
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Dval) VALUES( '12','No Unit',' ',' ','4.1/CLB'); -- No Unit
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Dval) VALUES( '13','No Unit',' ',' ','flashing on command'); -- No Unit
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Dval) VALUES( '14','No Unit',' ',' ','no zeros or NAN'); -- No Unit
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Dval) VALUES( '15','No Unit',' ',' ','present in DB'); -- No Unit
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '16','bar','0.08','0.3',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '17','rh%','20','50',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '18','dB','-90','-60',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '19','deg',' ','20',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '20','deg',' ','3.5',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '21','cps','250','1000',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '22','nS','23','32',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '23','&deg;C','15','25',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '24','&deg;C',' ','Ambient+13',@RGFLOAT);
INSERT INTO Measure_Type(M_T_id,Unit,Regex) VALUES( '25','No Unit',@RGMACAD); -- Mac ADDRESS
INSERT INTO Measure_Type(M_T_id,Unit,MMin,MMax,Regex) VALUES( '26','dB','-90','-60',@RGFLOAT);

CREATE TABLE `Test_Measures` (
  `T_id`  INT,  -- A Test type
  `M_id` INT,   -- Has 1,2,3,4 .... Measures
  `M_T_id` INT, -- Each Measure type
  `Name` Varchar(60), -- Measure Name
/*  `DAQ` Varchar(30) DEFAULT NULL, -- DAQ parameter Name
 */ 
	`TM_id`  varchar(60) AS (concat(`T_id`,'.',`M_id`)),

	PRIMARY KEY  (`T_id`,`M_id`),
	FOREIGN KEY  (`M_T_id`) REFERENCES  `Measure_Type` (`M_T_id`),
	FOREIGN KEY  (`T_id`) REFERENCES  `Test_Type` (`T_id`)
);
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '1','1','1','Helium Pressure');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '2','1','2','PRE-SPLICE POWER');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '2','2','2','POST-SPLICE POWER');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '2','3','3','Diff. POST-PRE POWER');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','1','25','MAC ADDRESS');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','2','11','Base-Ids correspond to integration DB?');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','3','11','Golden image SW version OK?');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','4','11','Golden image FW version OK?');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','5','11','Runtime image SW version OK?');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','7','11','Runtime image FW version OK?');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','8','12','AHRS/Compass FW version');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','9','5','Im');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','10','6','BOOT TIME');
/*INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name,DAQ) VALUES( '3','11','7','FPGA TEMPERATURE','TEMP_FPGA');*/
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','11','7','FPGA TEMPERATURE');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','12','7','CLB Temperature');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','13','8','SFP Temperature');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','19','23','Ambient Temperature');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','14','9','Piezo Noise');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','15','13','NANOBEACON');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','16','10','DOM optical output power');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','17','14','AHRS/Compass data');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '3','18','15','AHRS/Compass calibration values');

INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','1','25','MAC ADDRESS');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','2','11','Base-Ids correspond to integration DB?');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','3','11','Golden image SW version OK?');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','4','11','Golden image FW version OK?');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','5','11','Runtime image SW version OK?');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','7','11','Runtime image FW version OK?');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','8','12','AHRS/Compass FW version');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','9','16','DOM pressure gauge reading');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','10','5','Im');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','11','6','BOOT TIME');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','12','7','FPGA TEMPERATURE');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','13','7','CLB Temperature');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','14','24','SFP Temperature');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','22','23','Ambient Temperature');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','15','17','Humidity sensor');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','16','9','Piezo Noise');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','23','26','Piezo 30kHz line');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','24','10','DOM optical output power');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','17','19','Yaw shift');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','18','20','Residual 1');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','19','20','Residual 2');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','20','20','Residual 3');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '4','21','20','Residual 4');

INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','1','21','Dark count rate/cps 0');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','2','21','Dark count rate/cps 1');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','3','21','Dark count rate/cps 2');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','4','21','Dark count rate/cps 3');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','5','21','Dark count rate/cps 4');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','6','21','Dark count rate/cps 5');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','7','21','Dark count rate/cps 6');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','8','21','Dark count rate/cps 7');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','9','21','Dark count rate/cps 8');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','10','21','Dark count rate/cps 9');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','11','21','Dark count rate/cps 10');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','12','21','Dark count rate/cps 11');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','13','21','Dark count rate/cps 12');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','14','21','Dark count rate/cps 13');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','15','21','Dark count rate/cps 14');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','16','21','Dark count rate/cps 15');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','17','21','Dark count rate/cps 16');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','18','21','Dark count rate/cps 17');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','19','21','Dark count rate/cps 18');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','20','21','Dark count rate/cps 19');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','21','21','Dark count rate/cps 20');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','22','21','Dark count rate/cps 21');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','23','21','Dark count rate/cps 22');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','24','21','Dark count rate/cps 23');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','25','21','Dark count rate/cps 24');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','26','21','Dark count rate/cps 25');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','27','21','Dark count rate/cps 26');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','28','21','Dark count rate/cps 27');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','29','21','Dark count rate/cps 28');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','30','21','Dark count rate/cps 29');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '5','31','21','Dark count rate/cps 30');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','1','22','ToT 0');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','2','22','ToT 1');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','3','22','ToT 2');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','4','22','ToT 3');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','5','22','ToT 4');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','6','22','ToT 5');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','7','22','ToT 6');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','8','22','ToT 7');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','9','22','ToT 8');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','10','22','ToT 9');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','11','22','ToT 10');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','12','22','ToT 11');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','13','22','ToT 12');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','14','22','ToT 13');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','15','22','ToT 14');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','16','22','ToT 15');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','17','22','ToT 16');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','18','22','ToT 17');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','19','22','ToT 18');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','20','22','ToT 19');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','21','22','ToT 20');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','22','22','ToT 21');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','23','22','ToT 22');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','24','22','ToT 23');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','25','22','ToT 24');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','26','22','ToT 25');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','27','22','ToT 26');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','28','22','ToT 27');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','29','22','ToT 28');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','30','22','ToT 29');
INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name) VALUES( '6','31','22','ToT 30');

CREATE TABLE IF NOT EXISTS DAQ_Request  (
/*
0	A02085904	MAIL	Gilles.Bouvet@subatech.in2p3.fr

INSERT INTO Test_Measures(T_id,M_id,M_T_id,Name,DAQ) VALUES( '3','11','7','FPGA TEMPERATURE','TEMP_FPGA');
Actual http request that must be sent by the KM3DIA TO THE CU
http://fcmserver2.bo.infn.it:1302/mon/clb/outparams/TEMP_FPGA/1/3
*/
		Test_Type Int,
		Test_Measure Int,
		DAQName VARCHAR(30),
		DAQ_REQ VARCHAR(300),
		PRIMARY KEY (Test_Type,Test_Measure)
);
INSERT INTO DAQ_Request VALUES(3,11,'TEMP_FPGA',':1302/mon/clb/outparams/');
INSERT INTO DAQ_Request VALUES(4,12,'TEMP_FPGA',':1302/mon/clb/outparams/');



CREATE TABLE `Test` (
  `E_T_id` INT NOT NULL AUTO_INCREMENT, -- Effective Test Id 
  `T_id` INT,   -- Test Type
  `UPI` VARCHAR(300),
  `T_Time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, -- Creation Time will not be updated.
  PRIMARY KEY (`E_T_id`),
  FOREIGN KEY (`T_id`) REFERENCES `Test_Type` (`T_id`),
  FOREIGN KEY  (`UPI`) REFERENCES `ASSIGNED_DOM` (`UPI`)
);


CREATE TABLE `Measure` ( -- TIMESTAMP also single measurements ?
  `M_id` INT, -- For Each test Measure 1,2,3
  `E_T_id` INT, -- Before creating Measure rows a test rows is needed
  `VALUE` VARCHAR(30) DEFAULT NULL,
  `Conform` BOOLEAN DEFAULT TRUE,
  `B_id` INT,
  `KM3db_OID`  VARCHAR(30),
  `M_Time` TIMESTAMP,
  PRIMARY KEY (`M_id`, `E_T_id`)/*, --- NOT POSSIBLE TO INTRODUCE CONSTRAINT 
  FOREIGN KEY (`M_id`)  REFERENCES `Test_Measures` (`M_id`)*/,
  FOREIGN KEY (`B_id`) REFERENCES Bench (`B_id`),
  FOREIGN KEY (`E_T_id`)  REFERENCES `Test` (`E_T_id`),
  FOREIGN KEY (`KM3db_OID`)  REFERENCES `Operators` (`KM3db_OID`)
);









 
DELIMITER //
CREATE PROCEDURE Import_DOMUPI(IN in_UPI VARCHAR(300), IN in_SITE VARCHAR(30), OUT out_status VARCHAR(30))
    MODIFIES SQL DATA
BEGIN
DECLARE CONTINUE HANDLER FOR 1062 SET out_status='Duplicate Entry';
DECLARE CONTINUE HANDLER FOR 1242 SET out_status='more then one line';
SET out_status='OK';
  SET @IIsite = (SELECT SiteID FROM Site where KM3db_LOC_ID = in_SITE);
  SET out_status='OK';
IF @IIsite IS NOT NULL THEN 
   INSERT INTO ASSIGNED_DOM 
     (UPI,KM3db_LOC_ID,SiteID) 
    VALUES 
     (in_UPI,in_SITE,@IIsite);
END IF;
END//

CREATE PROCEDURE Import_Part(IN in_PBS VARCHAR(300), IN in_VARIANT VARCHAR(300), IN in_VERSION VARCHAR(300), IN in_SERIAL VARCHAR(300),IN in_SITE VARCHAR(300), OUT out_status VARCHAR(30))
    MODIFIES SQL DATA
BEGIN
DECLARE CONTINUE HANDLER FOR 1062 SET out_status='Duplicate Entry';
DECLARE CONTINUE HANDLER FOR 1452 SET out_status='PBS ERROR';
DECLARE CONTINUE HANDLER FOR 1242 SET out_status='more then one line';

  SET @IIsite = (SELECT SiteID FROM Site where KM3db_LOC_ID = in_SITE);
  
    
  SET out_status='OK';

IF @IIsite IS NOT NULL THEN
SET @IsBATCH = (SELECT ISBATCH FROM PBS where PBS = in_PBS );
IF @IsBATCH THEN
   INSERT INTO Parts 
     (PBS,Variant,Version,Serial,B_id) 
    VALUES 
     (in_PBS,in_VARIANT,in_VERSION,in_SERIAL,@IIsite);
ELSE 
SET @Exist = 
  (SELECT Pb_id FROM Parts where PBS = in_PBS AND Variant = in_VARIANT AND Version = in_VERSION  AND Serial = in_SERIAL and B_id = @IIsite );

IF @Exist IS NOT NULL THEN
UPDATE Parts set B_id=@IIsite where Pb_id=@Exist;
ELSE
INSERT INTO Parts 
     (PBS,Variant,Version,Serial,B_id) 
    VALUES 
     (in_PBS,in_VARIANT,in_VERSION,in_SERIAL,@IIsite);
END IF;
END IF;
END IF;
END//

DELIMITER ;

INSERT INTO ISites(SiteID,Town) VALUES( '1','Amsterdam');
INSERT INTO ISites(SiteID,Town) VALUES( '2','Athens');
INSERT INTO ISites(SiteID,Town) VALUES( '3','Catania');
INSERT INTO ISites(SiteID,Town) VALUES( '4','Erlangen');
INSERT INTO ISites(SiteID,Town) VALUES( '5','Nantes');
INSERT INTO ISites(SiteID,Town) VALUES( '6','Napoli');
INSERT INTO ISites(SiteID,Town) VALUES( '7','Rabat');
INSERT INTO ISites(SiteID,Town) VALUES( '8','Strasbourg');
INSERT INTO ISites(SiteID,Town) VALUES( '9','Bologna');

#DEFAULT STORAGE FOR EVERY SITE
INSERT INTO Bench(B_id,B_Details,SiteID,B_type) VALUES('1','STORAGE','1','1');
INSERT INTO Bench(B_id,B_Details,SiteID,B_type) VALUES('2','STORAGE','2','1');
INSERT INTO Bench(B_id,B_Details,SiteID,B_type) VALUES('3','STORAGE','3','1');
INSERT INTO Bench(B_id,B_Details,SiteID,B_type) VALUES('4','STORAGE','4','1');
INSERT INTO Bench(B_id,B_Details,SiteID,B_type) VALUES('5','STORAGE','5','1');
INSERT INTO Bench(B_id,B_Details,SiteID,B_type) VALUES('6','STORAGE','6','1');
INSERT INTO Bench(B_id,B_Details,SiteID,B_type) VALUES('7','STORAGE','7','1');
INSERT INTO Bench(B_id,B_Details,SiteID,B_type) VALUES('8','STORAGE','8','1');
INSERT INTO Bench(B_id,B_Details,SiteID,B_type) VALUES('9','STORAGE','9','1');


INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070002','Fisciano', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070003','Napoli', 6 );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070004','Marseille', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070005','Bologna', 9 );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070006','Catania', 3 );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070007','Bari', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070008','Roma', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070009','Erlangen', 4 );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070010','Amsterdam', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A02181353','Amsterdam', 1 );

INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070011','Pisa', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070012','Genova', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070013','Dublin', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070014','Gif-sur-Yvette', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00070015','Utrecht', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073003','Colmar', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073006','Unknown', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073482','Dublin', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073483','Paris Cedex 13', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073485','Mulhouse', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073486','Strasbourg', 8 );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073487','Bucharest', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073488','Paris 16', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073490','Athens', 2 );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073491','Valencia', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073493','Anavissos', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073494','Valencia', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073496','Moscow', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073497','La Seyne', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073498','Plouzane', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073499','Valencia', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073500','Athens', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073501','Gandia', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073503','Pozzuolo di Lerici', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073504','Buesum', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073505','Sheffield', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073507','Nicosia', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073508','Aberdeen', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073511','Athens', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073512','Aberdeen', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073513','Erlangen', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073514','Sheffield', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073515','Kiel', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073518','Anavissos', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073519','Nicosia', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073520','Athens', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073521','Ag. Paraskevi Attikis', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073523','Issy-les-Moulineaux Cedex', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073524','Patra', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073525','Venezia', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073526','Venezia', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073527','Erlangen', 4 );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073528','Liverpool', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073529','Madrid', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073530','Bucharest', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073531','Bamberg', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073532','Patra', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073535','Genova', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073536','Rome', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073537','Pisa', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073539','Frascati', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073541','Catania', 3 );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073543','Trieste', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073544','Sheffield', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073545','Leeds', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073546','Groningen', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073795','Italy', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073796','France', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073797','Greece', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00073859','Aubi?re Cedex', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074145','Leiden', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074146','Leiden', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074177','Amsterdam', 1 );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074180','Thessaloniki', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074181','Magurele', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074185','Amsterdam', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074186','Utrecht', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074187','t Horntje', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074192','Athens', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074193','Athens', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074208','W?rzburg', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074210','Gandia', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074218','Pisa', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074300','Den Burg', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074302','Utrecht', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074305','Leiden', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074307','Amsterdam', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00074313','W?rzburg', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00076356','M?nchen', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00076571','Paterna', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00076581','Herzogenaurach', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00076582','Herzogenaurach', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00076583','Herzogenaurach', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00079730','Delft', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00079731','Delft', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00110361','Vilanova i la Geltr?', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00238553','Warsaw', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00264945','Paris Cedex 13', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00267314','Livorno', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00350340','Oujda', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A00979351',"L'Aquila", NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01240337','Granada', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01240890','Tübingen', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01241075','Egaleo - Athens', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01241203','Rabat', 7 );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01241204','Rabat', 7 );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01288502','Caserta', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01288561','Granada', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01429618','Paris', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01429621','Paris', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01429624','Granada', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01429629','Paris', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01466441','Magurele', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01488193','Osservatorio Astrofisico di Catania', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01488194','Osservatorio Astrofisico di Catania', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01488195','Osservatorio Astrofisico di Catania', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01488196','Catania', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01488198','Catania', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01488199','Catania', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01488227','M?nster', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01495481','Moscow', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01504021','Forl?? ', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01523313','Iwata', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01546446','Nantes', 5 );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01550236','Tbilisi', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01550314','Marrakech', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01550325','Auckland Park', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01553392','Athens', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01553393','Athens', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01553394','Athens', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01553992','PIreaus', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01553993','PIreaus', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A01555211','Catania', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A02005240','Genova', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A02005269','Marseille', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A02005552','Johannesburg', NULL );
INSERT INTO Site(KM3db_LOC_ID,Town_Details, SiteID) VALUES( 'A02005553','Johannesburg', NULL );



INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('9','Select Bench','1');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('5','Create_Bench','1');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('5','Modify_User_Level','1');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('5','AddPart','1');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('9','SetUp Integration','1');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('9','Integration','0');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('5','Site Settings','1');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('11','Logout','1');

INSERT INTO PBS VALUES ('3.4','','DOM','0');
INSERT INTO PBS VALUES ('3.4.1','','Mechanics','0');
INSERT INTO PBS VALUES ('3.4.1.1','','Sphere Support','0');
INSERT INTO PBS VALUES ('3.4.1.2','','Interface','0');
INSERT INTO PBS VALUES ('3.4.1.2.1','','Interface with Rope','0');
INSERT INTO PBS VALUES ('3.4.1.2.2','','Penetrator','0');
INSERT INTO PBS VALUES ('3.4.1.2.3','','Sphere Connector','0');
INSERT INTO PBS VALUES ('3.4.1.2.3.1','','Metal Nut','0');
INSERT INTO PBS VALUES ('3.4.1.2.3.2','','Plastic Washer','0');
INSERT INTO PBS VALUES ('3.4.1.2.3.3','','Metal Washer','0');
INSERT INTO PBS VALUES ('3.4.1.3','','Sphere','0');
INSERT INTO PBS VALUES ('3.4.1.3.1','','Upper Hemisphere','0');
INSERT INTO PBS VALUES ('3.4.1.3.2','','Lower Hemisphere','1');
INSERT INTO PBS VALUES ('3.4.1.4','','PMT Support Structure','0');
INSERT INTO PBS VALUES ('3.4.1.4.1','','Plastic Support Parts','0');
INSERT INTO PBS VALUES ('3.4.1.4.1.1','','Support for Upper Hemisphere, string version','1');
INSERT INTO PBS VALUES ('3.4.1.4.1.2','','Support for Lower Hemisphere, string version','1');
INSERT INTO PBS VALUES ('3.4.1.4.1.3','','Support Part A','0');
INSERT INTO PBS VALUES ('3.4.1.4.1.4','','Support Part B','0');
INSERT INTO PBS VALUES ('3.4.1.4.1.5','','Support Part C','0');
INSERT INTO PBS VALUES ('3.4.1.4.2','','Plastic Bolt','0');
INSERT INTO PBS VALUES ('3.4.1.5','','Optical Interface / Gel','1');
INSERT INTO PBS VALUES ('3.4.1.5.1','','Component A','1');
INSERT INTO PBS VALUES ('3.4.1.5.2','','Component B','1');
INSERT INTO PBS VALUES ('3.4.1.6','','Cooling System','0');
INSERT INTO PBS VALUES ('3.4.1.6.1','','Cooling Mushroom','1');
INSERT INTO PBS VALUES ('3.4.1.6.2','','Cooling Bar','1');
INSERT INTO PBS VALUES ('3.4.1.6.3','','SFP Cooling Block','1');
INSERT INTO PBS VALUES ('3.4.1.7','','Pressure Gauge','1');
INSERT INTO PBS VALUES ('3.4.1.8','','Sphere Sealant','1');
INSERT INTO PBS VALUES ('3.4.1.9','','Tape','1');
INSERT INTO PBS VALUES ('3.4.2','','PMT Unit','0');
INSERT INTO PBS VALUES ('3.4.2.1','','Light Collection Device','1');
INSERT INTO PBS VALUES ('3.4.2.2','','PMT Base','0');
INSERT INTO PBS VALUES ('3.4.2.3','','PMT','0');
INSERT INTO PBS VALUES ('3.4.2.4','','Mu-Metal Cage','0');
INSERT INTO PBS VALUES ('3.4.3','','Electronics','0');
INSERT INTO PBS VALUES ('3.4.3.1','','Signal Collection Board (SCB)','0');
INSERT INTO PBS VALUES ('3.4.3.1.1','','Short SCB','0');
INSERT INTO PBS VALUES ('3.4.3.1.2','','Long SCB','0');
INSERT INTO PBS VALUES ('3.4.3.2','','Central Logic Board (CLB)','0');
INSERT INTO PBS VALUES ('3.4.3.2.1','','CLB Instrumentation Component','0');
INSERT INTO PBS VALUES ('3.4.3.2.1.1','','Temperature sensor','0');
INSERT INTO PBS VALUES ('3.4.3.2.1.2','','Humidity sensor','0');
INSERT INTO PBS VALUES ('3.4.3.2.2','','FPGA','0');
INSERT INTO PBS VALUES ('3.4.3.2.2.1','','Component','0');
INSERT INTO PBS VALUES ('3.4.3.2.2.2','','Software','0');
INSERT INTO PBS VALUES ('3.4.3.2.2.3','','Firmware','0');
INSERT INTO PBS VALUES ('3.4.3.2.3','','Optical Line Terminator (OLT)','0');
INSERT INTO PBS VALUES ('3.4.3.2.3.1','','SFP Optical Transceiver','1'); /*Now set to BATCH for phase 2*/
/* #INSERT INTO PBS VALUES ('3.4.3.2.3.1','TSFP','Tunable SFP Optical Transceiver','1');  Not using variant for phase 1 compatibility */
INSERT INTO PBS VALUES ('3.4.3.2.3.2','','Add-and-Drop Filter SC','1');
INSERT INTO PBS VALUES ('3.4.3.3','','Tiltmeter Board','0');
INSERT INTO PBS VALUES ('3.4.3.4','','Compass Board','0');
INSERT INTO PBS VALUES ('3.4.3.5','','Power Conversion Board','0');
INSERT INTO PBS VALUES ('3.4.3.6','','Acoustic Component','0');
INSERT INTO PBS VALUES ('3.4.3.6.1','','Acoustic Board','0');
INSERT INTO PBS VALUES ('3.4.3.6.2','','Acoustic Sensor','0');
INSERT INTO PBS VALUES ('3.4.3.6.3','','Pre-Amplifier','0');
INSERT INTO PBS VALUES ('3.4.3.7','','Nano Beacon','0');
INSERT INTO PBS VALUES ('3.4.3.7.1','','Nano Beacon Pulser','0');
INSERT INTO PBS VALUES ('3.4.3.7.2','','Nano Beacon Control Board','0');
INSERT INTO PBS VALUES ('3.4.3.8','',' LED Beacon','0');
INSERT INTO PBS VALUES ('3.4.3.9','',' Voltage Divider','0');
INSERT INTO PBS VALUES ('3.4.3.10','','FEM Board','0');
INSERT INTO PBS VALUES ('3.4.3.11','','Thermostat','0');
INSERT INTO PBS VALUES ('3.4.3.12','','PORFIDO','0');
INSERT INTO PBS VALUES ('3.4.4','',' Cables','0');
INSERT INTO PBS VALUES ('3.4.4.1','',' Optical Module Cable','0');
INSERT INTO PBS VALUES ('3.4.4.1.1','','Hose','0');
INSERT INTO PBS VALUES ('3.4.4.1.2','','Female in','0');
INSERT INTO PBS VALUES ('3.4.4.1.3','','Male in','0');
INSERT INTO PBS VALUES ('3.4.4.2','',' FEM Board Cable','0');
INSERT INTO PBS VALUES ('3.4.4.2.1','','Hose','0');
INSERT INTO PBS VALUES ('3.4.4.2.2','','Connector','0');
INSERT INTO PBS VALUES ('3.4.1.6.6','','FPGA Cooling Block','1');
INSERT INTO PBS VALUES ('3.4.1.6.7','','Faraday Plate','1');
INSERT INTO PBS VALUES ('3.4.1.1.1','','Collar','1');
INSERT INTO PBS VALUES ('3.4.1.6.11','','Cooling Gap-Pad SFP','1');
INSERT INTO PBS VALUES ('3.4.1.6.8','','Cooling Paste','1');
INSERT INTO PBS VALUES ('3.4.1.6.9','','Powerboard Cool-Pad','1');
INSERT INTO PBS VALUES ('3.4.2.5','',' HV Protection Fence','1');
INSERT INTO PBS VALUES ('3.4.3.2.3.3','','Splice Protector','0');
INSERT INTO PBS VALUES ('3.4.3.6.4','','Piezo Cable','0');
INSERT INTO PBS VALUES ('3.4.3.7.3','','Nano Beacon Cable','1');
INSERT INTO PBS VALUES ('3.4.1.4.1.8','','Core Bottom A','1');
INSERT INTO PBS VALUES ('3.4.2.6','',' PMT O-Ring','1');
INSERT INTO PBS VALUES ('3.4.3.6.5','','Piezo O-ring','1');
INSERT INTO PBS VALUES ('3.4.1.2.4','','Grounding Cable','1');
INSERT INTO PBS VALUES ('3.4.1.1.4','','Ti bolt gr 5 M6 x 25','1');
INSERT INTO PBS VALUES ('3.4.1.1.5','','Ti nut gr 5 M6','1');
INSERT INTO PBS VALUES ('3.4.1.4.1.6','','Core Top A','1');
INSERT INTO PBS VALUES ('3.4.1.13','','Nail Polish','0');
INSERT INTO PBS VALUES ('3.4.1.6.4','','Sliding Part','1');
INSERT INTO PBS VALUES ('3.4.1.4.1.9','','Core Bottom B','1');
INSERT INTO PBS VALUES ('3.4.1.4.1.7','','Core Top B','1');
INSERT INTO PBS VALUES ('3.4.1.1.2','','Backspacer','0');
INSERT INTO PBS VALUES ('3.4.1.11','','Silicon Mastic','1');
INSERT INTO PBS VALUES ('3.4.1.6.10','','Cooling Gap-Pad FPGA','1');
INSERT INTO PBS VALUES ('3.4.1.6.5','','Fiber Tray','1');
INSERT INTO PBS VALUES ('3.4.1.1.3','','Insert Foot','0');
INSERT INTO PBS VALUES ('3.4.1.10','','Vacuum Valve','1');
INSERT INTO PBS VALUES ('3.4.1.12','','Glue for Piezo','1');
INSERT INTO PBS VALUES ('3.4.5.1','','TOP HEMISPHERE','0');
INSERT INTO PBS VALUES ('3.4.5.2','','TOP SUPPORT','0');
INSERT INTO PBS VALUES ('3.4.5.3','','BOTTOM HEMISPHERE','0');
INSERT INTO PBS VALUES ('3.4.5.4','','BOTTOM SUPPORT','0');
INSERT INTO PBS VALUES ('3.4.5.1.1','','TOP HEMISPHERE HE-TESTED','0');
INSERT INTO PBS VALUES ('3.4.5.1.2','','TOP HEMISPHERE AFTER SPLICE TEST','0');
INSERT INTO PBS VALUES ('3.4.5.1.3','','INSTRUMENTED TOP HEMISPHERE','0');
INSERT INTO PBS VALUES ('3.4.5.3.1','','INSTRUMENTED BOTTOM HEMISPHERE','0');
INSERT INTO PBS VALUES ('3.4.5.3.2','','COUPLED INSTRUMENTED BOTTOM HEMISPHERE','0');
INSERT INTO PBS VALUES ('3.4.5.1.4','','TOP HEMISPHERE FINAL','0');
INSERT INTO PBS VALUES ('3.4.5.3.3','','BOTTOM HEMISPHERE FINAL','0');
INSERT INTO PBS VALUES ('3.4.5.5.5','','PRE A TEST DOM','0');
INSERT INTO DOMPOSPBS VALUES(38,'CLB (37)','3.4.3.2','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(40,'COOLBAR (39)','3.4.1.6.2','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(42,'C_BL_FPGA (41)','3.4.1.6.6','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(43,'C_BL_SFP (42)','3.4.1.6.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(44,'C_PAD_FPGA (43)','3.4.1.6.10','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(45,'C_PAD_P (44)','3.4.1.6.9','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(46,'C_PAD_S (45)','3.4.1.6.11','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(47,'FARPLATE (46)','3.4.1.6.7','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(48,'FIBTRAY (47)','3.4.1.6.5','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(49,'MUSHROOM (49)','3.4.1.6.1','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(50,'M_OP_GEL (50)','3.4.1.5','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(55,'ADD_AND_DROP (55)','3.4.3.2.3.2','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(56,'PBOARD (56)','3.4.3.5','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(57,'PENETRAT (57)','3.4.1.2.2','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(64,'SFP (64)','3.4.3.2.3.1','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(66,'TOP_SPHE (66)','3.4.1.3.1','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(1,'PM_T00_F4 (0)','3.4.2.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(2,'PM_T01_E5 (1)','3.4.2.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(3,'PM_T02_E4 (2)','3.4.2.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(4,'PM_T03_E3 (3)','3.4.2.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(5,'PM_T04_F3 (4)','3.4.2.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(6,'PM_T05_F5 (5)','3.4.2.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(7,'PM_T06_E2 (6)','3.4.2.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(8,'PM_T07_F6 (7)','3.4.2.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(9,'PM_T08_F2 (8)','3.4.2.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(10,'PM_T09_F1 (9)','3.4.2.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(11,'PM_T10_E1 (10)','3.4.2.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(12,'PM_T11_E6 (11)','3.4.2.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(51,'NANOBEAC (51)','3.4.3.7.1','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(52,'NBCABLE (52)','3.4.3.7.3','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(54,'OCTOP_S (54)','3.4.3.1.1','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(61,'PRESGAUG (61)','3.4.1.7','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(67,'TOP_SUPP (67)','3.4.1.4.1.1','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(68,'T_HVFENC (68)','3.4.2.5','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(70,'T_ORING (70)','3.4.2.6','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(71,'T_REFLEC (71)','3.4.2.1','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(69,'T_OP_GEL (69)','3.4.1.5','','3.4.5.1.4');
INSERT INTO DOMPOSPBS VALUES(72,'T_SIL_MA (72)','3.4.1.11','','3.4.5.1.4');


INSERT INTO DOMPOSPBS VALUES(32,'BOT_SPHE (31)','3.4.1.3.2','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(58,'PIEZO (58)','3.4.3.6.2','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(59,'PI_GLUE (59)','3.4.1.12','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(60,'PI_ORING (60)','3.4.3.6.5','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(13,'PM_B00_D1 (12)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(14,'PM_B01_C1 (13)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(15,'PM_B02_B1 (14)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(16,'PM_B03_D2 (15)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(17,'PM_B04_D6 (16)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(18,'PM_B05_C6 (17)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(19,'PM_B06_B6 (18)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(20,'PM_B07_B2 (19)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(21,'PM_B08_C5 (20)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(22,'PM_B09_C2 (21)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(23,'PM_B10_A1 (22)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(24,'PM_B11_D3 (23)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(25,'PM_B12_B4 (24)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(26,'PM_B13_B3 (25)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(27,'PM_B14_B5 (26)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(28,'PM_B15_D5 (27)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(29,'PM_B16_C4 (28)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(30,'PM_B17_C3 (29)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(31,'PM_B18_D4 (30)','3.4.2.3','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(33,'BOT_SUPP (32)','3.4.1.4.1.2','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(34,'B_HVFENC (33)','3.4.2.5','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(36,'B_ORING (35)','3.4.2.6','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(37,'B_REFLEC (36)','3.4.2.1','','3.4.5.1.0'); 
INSERT INTO DOMPOSPBS VALUES(53,'OCTOP_L (53)','3.4.3.1.2','','3.4.5.1.0');
INSERT INTO DOMPOSPBS VALUES(65,'SLID_PAR (65)','3.4.1.6.4','','3.4.5.1.0');


INSERT INTO DOMPOSPBS VALUES(32,'BOT_SPHE (31)','3.4.1.3.2','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(58,'PIEZO (58)','3.4.3.6.2','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(59,'PI_GLUE (59)','3.4.1.12','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(60,'PI_ORING (60)','3.4.3.6.5','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(13,'PM_B00_D1 (12)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(14,'PM_B01_C1 (13)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(15,'PM_B02_B1 (14)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(16,'PM_B03_D2 (15)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(17,'PM_B04_D6 (16)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(18,'PM_B05_C6 (17)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(19,'PM_B06_B6 (18)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(20,'PM_B07_B2 (19)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(21,'PM_B08_C5 (20)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(22,'PM_B09_C2 (21)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(23,'PM_B10_A1 (22)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(24,'PM_B11_D3 (23)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(25,'PM_B12_B4 (24)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(26,'PM_B13_B3 (25)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(27,'PM_B14_B5 (26)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(28,'PM_B15_D5 (27)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(29,'PM_B16_C4 (28)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(30,'PM_B17_C3 (29)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(31,'PM_B18_D4 (30)','3.4.2.3','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(33,'BOT_SUPP (32)','3.4.1.4.1.2','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(34,'B_HVFENC (33)','3.4.2.5','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(36,'B_ORING (35)','3.4.2.6','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(37,'B_REFLEC (36)','3.4.2.1','','3.4.5.3.3'); 
INSERT INTO DOMPOSPBS VALUES(53,'OCTOP_L (53)','3.4.3.1.2','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(65,'SLID_PAR (65)','3.4.1.6.4','','3.4.5.3.3');
INSERT INTO DOMPOSPBS VALUES(35,'B_OP_GEL (34)','3.4.1.5','','3.4.5.3.3');


/* */
INSERT INTO DOMPOSPBS VALUES(1,'PM_T00_F4 (0)','3.4.2.3','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(2,'PM_T01_E5 (1)','3.4.2.3','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(3,'PM_T02_E4 (2)','3.4.2.3','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(4,'PM_T03_E3 (3)','3.4.2.3','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(5,'PM_T04_F3 (4)','3.4.2.3','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(6,'PM_T05_F5 (5)','3.4.2.3','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(7,'PM_T06_E2 (6)','3.4.2.3','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(8,'PM_T07_F6 (7)','3.4.2.3','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(9,'PM_T08_F2 (8)','3.4.2.3','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(10,'PM_T09_F1 (9)','3.4.2.3','','3.4.5.2');

INSERT INTO DOMPOSPBS VALUES(11,'PM_T10_E1 (10)','3.4.2.3','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(12,'PM_T11_E6 (11)','3.4.2.3','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(61,'PRESGAUG (61)','3.4.1.7','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(67,'TOP_SUPP (67)','3.4.1.4.1.1','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(68,'T_HVFENC (68)','3.4.2.5','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(70,'T_ORING (70)','3.4.2.6','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(71,'T_REFLEC (71)','3.4.2.1','','3.4.5.2');


INSERT INTO DOMPOSPBS VALUES(13,'PM_B00_D1 (12)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(14,'PM_B01_C1 (13)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(15,'PM_B02_B1 (14)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(16,'PM_B03_D2 (15)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(17,'PM_B04_D6 (16)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(18,'PM_B05_C6 (17)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(19,'PM_B06_B6 (18)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(20,'PM_B07_B2 (19)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(21,'PM_B08_C5 (20)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(22,'PM_B09_C2 (21)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(23,'PM_B10_A1 (22)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(24,'PM_B11_D3 (23)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(25,'PM_B12_B4 (24)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(26,'PM_B13_B3 (25)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(27,'PM_B14_B5 (26)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(28,'PM_B15_D5 (27)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(29,'PM_B16_C4 (28)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(30,'PM_B17_C3 (29)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(31,'PM_B18_D4 (30)','3.4.2.3','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(32,'BOT_SPHE (31)','3.4.1.3.2','','3.4.5.3');
INSERT INTO DOMPOSPBS VALUES(33,'BOT_SUPP (32)','3.4.1.4.1.2','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(34,'B_HVFENC (33)','3.4.2.5','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(35,'B_OP_GEL (34)','3.4.1.5','','3.4.5.5.4');
INSERT INTO DOMPOSPBS VALUES(36,'B_ORING (35)','3.4.2.6','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(37,'B_REFLEC (36)','3.4.2.1','','3.4.5.4'); 
INSERT INTO DOMPOSPBS VALUES(38,'CLB (37)','3.4.3.2','','3.4.5.1.1');
INSERT INTO DOMPOSPBS VALUES(38,'CLB (37)','3.4.3.2','','3.4.5.1');

INSERT INTO DOMPOSPBS VALUES(39,'COLLAR (38)','3.4.1.1.1','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(40,'COOLBAR (39)','3.4.1.6.2','','3.4.5.1.1');
INSERT INTO DOMPOSPBS VALUES(40,'COOLBAR (39)','3.4.1.6.2','','3.4.5.1'); /*NEW */
INSERT INTO DOMPOSPBS VALUES(41,'COOLPAST (40)','3.4.1.6.8','','');
INSERT INTO DOMPOSPBS VALUES(42,'C_BL_FPGA (41)','3.4.1.6.6','','3.4.5.1.1');
INSERT INTO DOMPOSPBS VALUES(42,'C_BL_FPGA (41)','3.4.1.6.6','','3.4.5.1');

INSERT INTO DOMPOSPBS VALUES(43,'C_BL_SFP (42)','3.4.1.6.3','','3.4.5.1.1');
INSERT INTO DOMPOSPBS VALUES(43,'C_BL_SFP (42)','3.4.1.6.3','','3.4.5.1');

INSERT INTO DOMPOSPBS VALUES(44,'C_PAD_FPGA (43)','3.4.1.6.10','','3.4.5.1.1');
INSERT INTO DOMPOSPBS VALUES(44,'C_PAD_FPGA (43)','3.4.1.6.10','','3.4.5.1');

INSERT INTO DOMPOSPBS VALUES(45,'C_PAD_P (44)','3.4.1.6.9','','3.4.5.1.1');
INSERT INTO DOMPOSPBS VALUES(45,'C_PAD_P (44)','3.4.1.6.9','','3.4.5.1');

INSERT INTO DOMPOSPBS VALUES(46,'C_PAD_S (45)','3.4.1.6.11','','3.4.5.1.1');
INSERT INTO DOMPOSPBS VALUES(46,'C_PAD_S (45)','3.4.1.6.11','','3.4.5.1');

INSERT INTO DOMPOSPBS VALUES(47,'FARPLATE (46)','3.4.1.6.7','','3.4.5.1.1');
INSERT INTO DOMPOSPBS VALUES(47,'FARPLATE (46)','3.4.1.6.7','','3.4.5.1');

INSERT INTO DOMPOSPBS VALUES(48,'FIBTRAY (47)','3.4.1.6.5','','3.4.5.1.1');
INSERT INTO DOMPOSPBS VALUES(49,'MUSHROOM (49)','3.4.1.6.1','','3.4.5.1');
INSERT INTO DOMPOSPBS VALUES(49,'MUSHROOM (49)','3.4.1.6.1','','3.4.5.1.1');
INSERT INTO DOMPOSPBS VALUES(50,'M_OP_GEL (50)','3.4.1.5','','3.4.5.1');
INSERT INTO DOMPOSPBS VALUES(50,'M_OP_GEL (50)','3.4.1.5','','3.4.5.1.1');
INSERT INTO DOMPOSPBS VALUES(51,'NANOBEAC (51)','3.4.3.7.1','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(52,'NBCABLE (52)','3.4.3.7.3','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(53,'OCTOP_L (53)','3.4.3.1.2','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(54,'OCTOP_S (54)','3.4.3.1.1','','3.4.5.2');
INSERT INTO DOMPOSPBS VALUES(55,'ADD_AND_DROP (55)','3.4.3.2.3.2','','3.4.5.1.1');

INSERT INTO DOMPOSPBS VALUES(56,'PBOARD (56)','3.4.3.5','','3.4.5.1.1');
INSERT INTO DOMPOSPBS VALUES(56,'PBOARD (56)','3.4.3.5','','3.4.5.1');

INSERT INTO DOMPOSPBS VALUES(57,'PENETRAT (57)','3.4.1.2.2','','3.4.5.1');
INSERT INTO DOMPOSPBS VALUES(57,'PENETRAT (57)','3.4.1.2.2','','3.4.5.1.1');


INSERT INTO DOMPOSPBS VALUES(58,'PIEZO (58)','3.4.3.6.2','','3.4.5.3');
INSERT INTO DOMPOSPBS VALUES(59,'PI_GLUE (59)','3.4.1.12','','3.4.5.3');
INSERT INTO DOMPOSPBS VALUES(60,'PI_ORING (60)','3.4.3.6.5','','3.4.5.4');

INSERT INTO DOMPOSPBS VALUES(62,'SC_TAPE (62)','3.4.1.9','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(63,'SEALANT (63)','3.4.1.8','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(64,'SFP (64)','3.4.3.2.3.1','','3.4.5.1.1');

INSERT INTO DOMPOSPBS VALUES(64,'SFP (64)','3.4.3.2.3.1','','3.4.5.1');/*NEW */
INSERT INTO DOMPOSPBS VALUES(69,'T_OP_GEL (69)','3.4.1.5','','3.4.5.5.4');
INSERT INTO DOMPOSPBS VALUES(65,'SLID_PAR (65)','3.4.1.6.4','','3.4.5.4');
INSERT INTO DOMPOSPBS VALUES(66,'TOP_SPHE (66)','3.4.1.3.1','','3.4.5.1');
INSERT INTO DOMPOSPBS VALUES(66,'TOP_SPHE (66)','3.4.1.3.1','','3.4.5.1.1');


INSERT INTO DOMPOSPBS VALUES(72,'T_SIL_MA (72)','3.4.1.11','','3.4.5.5.3');
INSERT INTO DOMPOSPBS VALUES(73,'VACVALVE (73)','3.4.1.10','','');
INSERT INTO DOMPOSPBS VALUES(74,'BOLT (74)','3.4.1.1.4','','3.4.5.5.5');

INSERT INTO DOMPOSPBS VALUES(38,'CLB (37)','3.4.3.2','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(40,'COOLBAR (39)','3.4.1.6.2','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(42,'C_BL_FPGA (41)','3.4.1.6.6','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(43,'C_BL_SFP (42)','3.4.1.6.3','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(44,'C_PAD_FPGA (43)','3.4.1.6.10','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(45,'C_PAD_P (44)','3.4.1.6.9','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(46,'C_PAD_S (45)','3.4.1.6.11','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(47,'FARPLATE (46)','3.4.1.6.7','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(48,'FIBTRAY (47)','3.4.1.6.5','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(49,'MUSHROOM (49)','3.4.1.6.1','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(50,'M_OP_GEL (50)','3.4.1.5','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(55,'ADD_AND_DROP (55)','3.4.3.2.3.2','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(56,'PBOARD (56)','3.4.3.5','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(57,'PENETRAT (57)','3.4.1.2.2','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(64,'SFP (64)','3.4.3.2.3.1','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(66,'TOP_SPHE (66)','3.4.1.3.1','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(1,'PM_T00_F4 (0)','3.4.2.3','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(2,'PM_T01_E5 (1)','3.4.2.3','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(3,'PM_T02_E4 (2)','3.4.2.3','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(4,'PM_T03_E3 (3)','3.4.2.3','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(5,'PM_T04_F3 (4)','3.4.2.3','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(6,'PM_T05_F5 (5)','3.4.2.3','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(7,'PM_T06_E2 (6)','3.4.2.3','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(8,'PM_T07_F6 (7)','3.4.2.3','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(9,'PM_T08_F2 (8)','3.4.2.3','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(10,'PM_T09_F1 (9)','3.4.2.3','','3.4.5.1.2');
/* */
INSERT INTO DOMPOSPBS VALUES(11,'PM_T10_E1 (10)','3.4.2.3','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(12,'PM_T11_E6 (11)','3.4.2.3','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(51,'NANOBEAC (51)','3.4.3.7.1','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(52,'NBCABLE (52)','3.4.3.7.3','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(54,'OCTOP_S (54)','3.4.3.1.1','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(61,'PRESGAUG (61)','3.4.1.7','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(67,'TOP_SUPP (67)','3.4.1.4.1.1','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(68,'T_HVFENC (68)','3.4.2.5','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(70,'T_ORING (70)','3.4.2.6','','3.4.5.1.2');
INSERT INTO DOMPOSPBS VALUES(71,'T_REFLEC (71)','3.4.2.1','','3.4.5.1.2');


INSERT INTO DOMPOSPBS VALUES(38,'CLB (37)','3.4.3.2','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(40,'COOLBAR (39)','3.4.1.6.2','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(42,'C_BL_FPGA (41)','3.4.1.6.6','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(43,'C_BL_SFP (42)','3.4.1.6.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(44,'C_PAD_FPGA (43)','3.4.1.6.10','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(45,'C_PAD_P (44)','3.4.1.6.9','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(46,'C_PAD_S (45)','3.4.1.6.11','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(47,'FARPLATE (46)','3.4.1.6.7','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(48,'FIBTRAY (47)','3.4.1.6.5','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(49,'MUSHROOM (49)','3.4.1.6.1','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(50,'M_OP_GEL (50)','3.4.1.5','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(55,'ADD_AND_DROP (55)','3.4.3.2.3.2','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(56,'PBOARD (56)','3.4.3.5','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(57,'PENETRAT (57)','3.4.1.2.2','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(64,'SFP (64)','3.4.3.2.3.1','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(66,'TOP_SPHE (66)','3.4.1.3.1','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(1,'PM_T00_F4 (0)','3.4.2.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(2,'PM_T01_E5 (1)','3.4.2.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(3,'PM_T02_E4 (2)','3.4.2.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(4,'PM_T03_E3 (3)','3.4.2.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(5,'PM_T04_F3 (4)','3.4.2.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(6,'PM_T05_F5 (5)','3.4.2.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(7,'PM_T06_E2 (6)','3.4.2.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(8,'PM_T07_F6 (7)','3.4.2.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(9,'PM_T08_F2 (8)','3.4.2.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(10,'PM_T09_F1 (9)','3.4.2.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(11,'PM_T10_E1 (10)','3.4.2.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(12,'PM_T11_E6 (11)','3.4.2.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(51,'NANOBEAC (51)','3.4.3.7.1','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(52,'NBCABLE (52)','3.4.3.7.3','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(54,'OCTOP_S (54)','3.4.3.1.1','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(61,'PRESGAUG (61)','3.4.1.7','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(67,'TOP_SUPP (67)','3.4.1.4.1.1','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(68,'T_HVFENC (68)','3.4.2.5','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(70,'T_ORING (70)','3.4.2.6','','3.4.5.1.3');
INSERT INTO DOMPOSPBS VALUES(71,'T_REFLEC (71)','3.4.2.1','','3.4.5.1.3');

INSERT INTO DOMPOSPBS VALUES(32,'BOT_SPHE (31)','3.4.1.3.2','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(58,'PIEZO (58)','3.4.3.6.2','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(59,'PI_GLUE (59)','3.4.1.12','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(60,'PI_ORING (60)','3.4.3.6.5','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(13,'PM_B00_D1 (12)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(14,'PM_B01_C1 (13)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(15,'PM_B02_B1 (14)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(16,'PM_B03_D2 (15)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(17,'PM_B04_D6 (16)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(18,'PM_B05_C6 (17)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(19,'PM_B06_B6 (18)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(20,'PM_B07_B2 (19)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(21,'PM_B08_C5 (20)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(22,'PM_B09_C2 (21)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(23,'PM_B10_A1 (22)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(24,'PM_B11_D3 (23)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(25,'PM_B12_B4 (24)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(26,'PM_B13_B3 (25)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(27,'PM_B14_B5 (26)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(28,'PM_B15_D5 (27)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(29,'PM_B16_C4 (28)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(30,'PM_B17_C3 (29)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(31,'PM_B18_D4 (30)','3.4.2.3','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(33,'BOT_SUPP (32)','3.4.1.4.1.2','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(34,'B_HVFENC (33)','3.4.2.5','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(36,'B_ORING (35)','3.4.2.6','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(37,'B_REFLEC (36)','3.4.2.1','','3.4.5.3.1'); 
INSERT INTO DOMPOSPBS VALUES(53,'OCTOP_L (53)','3.4.3.1.2','','3.4.5.3.1');
INSERT INTO DOMPOSPBS VALUES(65,'SLID_PAR (65)','3.4.1.6.4','','3.4.5.3.1');


INSERT INTO DOMPOSPBS VALUES(32,'BOT_SPHE (31)','3.4.1.3.2','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(58,'PIEZO (58)','3.4.3.6.2','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(59,'PI_GLUE (59)','3.4.1.12','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(60,'PI_ORING (60)','3.4.3.6.5','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(13,'PM_B00_D1 (12)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(14,'PM_B01_C1 (13)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(15,'PM_B02_B1 (14)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(16,'PM_B03_D2 (15)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(17,'PM_B04_D6 (16)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(18,'PM_B05_C6 (17)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(19,'PM_B06_B6 (18)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(20,'PM_B07_B2 (19)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(21,'PM_B08_C5 (20)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(22,'PM_B09_C2 (21)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(23,'PM_B10_A1 (22)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(24,'PM_B11_D3 (23)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(25,'PM_B12_B4 (24)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(26,'PM_B13_B3 (25)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(27,'PM_B14_B5 (26)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(28,'PM_B15_D5 (27)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(29,'PM_B16_C4 (28)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(30,'PM_B17_C3 (29)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(31,'PM_B18_D4 (30)','3.4.2.3','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(33,'BOT_SUPP (32)','3.4.1.4.1.2','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(34,'B_HVFENC (33)','3.4.2.5','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(36,'B_ORING (35)','3.4.2.6','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(37,'B_REFLEC (36)','3.4.2.1','','3.4.5.3.2'); 
INSERT INTO DOMPOSPBS VALUES(53,'OCTOP_L (53)','3.4.3.1.2','','3.4.5.3.2');
INSERT INTO DOMPOSPBS VALUES(65,'SLID_PAR (65)','3.4.1.6.4','','3.4.5.3.2');


INSERT INTO DOMPOSPBS VALUES(35,'B_OP_GEL (34)','3.4.1.5','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(69,'T_OP_GEL (69)','3.4.1.5','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(72,'T_SIL_MA (72)','3.4.1.11','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(32,'BOT_SPHE (31)','3.4.1.3.2','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(58,'PIEZO (58)','3.4.3.6.2','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(59,'PI_GLUE (59)','3.4.1.12','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(60,'PI_ORING (60)','3.4.3.6.5','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(13,'PM_B00_D1 (12)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(14,'PM_B01_C1 (13)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(15,'PM_B02_B1 (14)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(16,'PM_B03_D2 (15)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(17,'PM_B04_D6 (16)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(18,'PM_B05_C6 (17)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(19,'PM_B06_B6 (18)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(20,'PM_B07_B2 (19)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(21,'PM_B08_C5 (20)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(22,'PM_B09_C2 (21)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(23,'PM_B10_A1 (22)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(24,'PM_B11_D3 (23)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(25,'PM_B12_B4 (24)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(26,'PM_B13_B3 (25)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(27,'PM_B14_B5 (26)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(28,'PM_B15_D5 (27)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(29,'PM_B16_C4 (28)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(30,'PM_B17_C3 (29)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(31,'PM_B18_D4 (30)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(33,'BOT_SUPP (32)','3.4.1.4.1.2','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(34,'B_HVFENC (33)','3.4.2.5','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(36,'B_ORING (35)','3.4.2.6','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(37,'B_REFLEC (36)','3.4.2.1','','3.4.5.5.5'); 
INSERT INTO DOMPOSPBS VALUES(53,'OCTOP_L (53)','3.4.3.1.2','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(65,'SLID_PAR (65)','3.4.1.6.4','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(38,'CLB (37)','3.4.3.2','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(40,'COOLBAR (39)','3.4.1.6.2','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(42,'C_BL_FPGA (41)','3.4.1.6.6','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(43,'C_BL_SFP (42)','3.4.1.6.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(44,'C_PAD_FPGA (43)','3.4.1.6.10','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(45,'C_PAD_P (44)','3.4.1.6.9','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(46,'C_PAD_S (45)','3.4.1.6.11','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(47,'FARPLATE (46)','3.4.1.6.7','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(48,'FIBTRAY (47)','3.4.1.6.5','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(49,'MUSHROOM (49)','3.4.1.6.1','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(50,'M_OP_GEL (50)','3.4.1.5','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(55,'ADD_AND_DROP (55)','3.4.3.2.3.2','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(56,'PBOARD (56)','3.4.3.5','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(57,'PENETRAT (57)','3.4.1.2.2','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(64,'SFP (64)','3.4.3.2.3.1','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(66,'TOP_SPHE (66)','3.4.1.3.1','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(1,'PM_T00_F4 (0)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(2,'PM_T01_E5 (1)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(3,'PM_T02_E4 (2)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(4,'PM_T03_E3 (3)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(5,'PM_T04_F3 (4)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(6,'PM_T05_F5 (5)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(7,'PM_T06_E2 (6)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(8,'PM_T07_F6 (7)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(9,'PM_T08_F2 (8)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(10,'PM_T09_F1 (9)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(11,'PM_T10_E1 (10)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(12,'PM_T11_E6 (11)','3.4.2.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(51,'NANOBEAC (51)','3.4.3.7.1','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(52,'NBCABLE (52)','3.4.3.7.3','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(54,'OCTOP_S (54)','3.4.3.1.1','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(61,'PRESGAUG (61)','3.4.1.7','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(67,'TOP_SUPP (67)','3.4.1.4.1.1','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(68,'T_HVFENC (68)','3.4.2.5','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(70,'T_ORING (70)','3.4.2.6','','3.4.5.5.5');
INSERT INTO DOMPOSPBS VALUES(71,'T_REFLEC (71)','3.4.2.1','','3.4.5.5.5');

/*COMPLETE DOM */
INSERT INTO DOMPOSPBS VALUES(39,'COLLAR (38)','3.4.1.1.1','','3.4');
INSERT INTO DOMPOSPBS VALUES(62,'SC_TAPE (62)','3.4.1.9','','3.4');
INSERT INTO DOMPOSPBS VALUES(63,'SEALANT (63)','3.4.1.8','','3.4');
INSERT INTO DOMPOSPBS VALUES(74,'BOLT (74)','3.4.1.1.4','','3.4');
INSERT INTO DOMPOSPBS VALUES(35,'B_OP_GEL (34)','3.4.1.5','','3.4');
INSERT INTO DOMPOSPBS VALUES(69,'T_OP_GEL (69)','3.4.1.5','','3.4');
INSERT INTO DOMPOSPBS VALUES(72,'T_SIL_MA (72)','3.4.1.11','','3.4');
INSERT INTO DOMPOSPBS VALUES(32,'BOT_SPHE (31)','3.4.1.3.2','','3.4');
INSERT INTO DOMPOSPBS VALUES(58,'PIEZO (58)','3.4.3.6.2','','3.4');
INSERT INTO DOMPOSPBS VALUES(59,'PI_GLUE (59)','3.4.1.12','','3.4');
INSERT INTO DOMPOSPBS VALUES(60,'PI_ORING (60)','3.4.3.6.5','','3.4');
INSERT INTO DOMPOSPBS VALUES(13,'PM_B00_D1 (12)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(14,'PM_B01_C1 (13)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(15,'PM_B02_B1 (14)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(16,'PM_B03_D2 (15)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(17,'PM_B04_D6 (16)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(18,'PM_B05_C6 (17)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(19,'PM_B06_B6 (18)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(20,'PM_B07_B2 (19)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(21,'PM_B08_C5 (20)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(22,'PM_B09_C2 (21)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(23,'PM_B10_A1 (22)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(24,'PM_B11_D3 (23)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(25,'PM_B12_B4 (24)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(26,'PM_B13_B3 (25)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(27,'PM_B14_B5 (26)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(28,'PM_B15_D5 (27)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(29,'PM_B16_C4 (28)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(30,'PM_B17_C3 (29)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(31,'PM_B18_D4 (30)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(33,'BOT_SUPP (32)','3.4.1.4.1.2','','3.4');
INSERT INTO DOMPOSPBS VALUES(34,'B_HVFENC (33)','3.4.2.5','','3.4');
INSERT INTO DOMPOSPBS VALUES(36,'B_ORING (35)','3.4.2.6','','3.4');
INSERT INTO DOMPOSPBS VALUES(37,'B_REFLEC (36)','3.4.2.1','','3.4'); 
INSERT INTO DOMPOSPBS VALUES(53,'OCTOP_L (53)','3.4.3.1.2','','3.4');
INSERT INTO DOMPOSPBS VALUES(65,'SLID_PAR (65)','3.4.1.6.4','','3.4');
INSERT INTO DOMPOSPBS VALUES(38,'CLB (37)','3.4.3.2','','3.4');
INSERT INTO DOMPOSPBS VALUES(40,'COOLBAR (39)','3.4.1.6.2','','3.4');
INSERT INTO DOMPOSPBS VALUES(42,'C_BL_FPGA (41)','3.4.1.6.6','','3.4');
INSERT INTO DOMPOSPBS VALUES(43,'C_BL_SFP (42)','3.4.1.6.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(44,'C_PAD_FPGA (43)','3.4.1.6.10','','3.4');
INSERT INTO DOMPOSPBS VALUES(45,'C_PAD_P (44)','3.4.1.6.9','','3.4');
INSERT INTO DOMPOSPBS VALUES(46,'C_PAD_S (45)','3.4.1.6.11','','3.4');
INSERT INTO DOMPOSPBS VALUES(47,'FARPLATE (46)','3.4.1.6.7','','3.4');
INSERT INTO DOMPOSPBS VALUES(48,'FIBTRAY (47)','3.4.1.6.5','','3.4');
INSERT INTO DOMPOSPBS VALUES(49,'MUSHROOM (49)','3.4.1.6.1','','3.4');
INSERT INTO DOMPOSPBS VALUES(50,'M_OP_GEL (50)','3.4.1.5','','3.4');
INSERT INTO DOMPOSPBS VALUES(55,'ADD_AND_DROP (55)','3.4.3.2.3.2','','3.4');
INSERT INTO DOMPOSPBS VALUES(56,'PBOARD (56)','3.4.3.5','','3.4');
INSERT INTO DOMPOSPBS VALUES(57,'PENETRAT (57)','3.4.1.2.2','','3.4');
INSERT INTO DOMPOSPBS VALUES(64,'SFP (64)','3.4.3.2.3.1','','3.4');
INSERT INTO DOMPOSPBS VALUES(66,'TOP_SPHE (66)','3.4.1.3.1','','3.4');
INSERT INTO DOMPOSPBS VALUES(1,'PM_T00_F4 (0)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(2,'PM_T01_E5 (1)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(3,'PM_T02_E4 (2)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(4,'PM_T03_E3 (3)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(5,'PM_T04_F3 (4)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(6,'PM_T05_F5 (5)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(7,'PM_T06_E2 (6)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(8,'PM_T07_F6 (7)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(9,'PM_T08_F2 (8)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(10,'PM_T09_F1 (9)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(11,'PM_T10_E1 (10)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(12,'PM_T11_E6 (11)','3.4.2.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(51,'NANOBEAC (51)','3.4.3.7.1','','3.4');
INSERT INTO DOMPOSPBS VALUES(52,'NBCABLE (52)','3.4.3.7.3','','3.4');
INSERT INTO DOMPOSPBS VALUES(54,'OCTOP_S (54)','3.4.3.1.1','','3.4');
INSERT INTO DOMPOSPBS VALUES(61,'PRESGAUG (61)','3.4.1.7','','3.4');
INSERT INTO DOMPOSPBS VALUES(67,'TOP_SUPP (67)','3.4.1.4.1.1','','3.4');
INSERT INTO DOMPOSPBS VALUES(68,'T_HVFENC (68)','3.4.2.5','','3.4');
INSERT INTO DOMPOSPBS VALUES(70,'T_ORING (70)','3.4.2.6','','3.4');
INSERT INTO DOMPOSPBS VALUES(71,'T_REFLEC (71)','3.4.2.1','','3.4');


