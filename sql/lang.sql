DROP DATABASE KM3LANG;
CREATE DATABASE KM3LANG
DEFAULT CHARACTER SET utf8 
DEFAULT COLLATE utf8_general_ci;
USE KM3LANG;
DROP USER km3WWW;
CREATE USER  km3WWW  IDENTIFIED BY  'pyrosoma' ;
GRANT SELECT ON KM3LANG.* TO  km3WWW ;
CREATE TABLE `Strings` (
  `USI`  INT NOT NULL AUTO_INCREMENT,
  `Lang` VARCHAR(30),
  `Page_ID` INT,
  `STRING` VARCHAR(3000),
  `STR_order` INT,
  PRIMARY KEY (`USI`)
  
);


CREATE TABLE `Languages` (
  `Lang` VARCHAR(30),
  `Description` VARCHAR(300),
  KEY `Pk` (`Lang`)
);
CREATE TABLE `VOICES` (
  `Lang` VARCHAR(30),
  `Description` VARCHAR(300),
  KEY `Pk` (`Lang`)
);
CREATE TABLE `Page` (
  `Page_ID` INT,
  PRIMARY KEY (`Page_ID`)
);

ALTER TABLE Strings ADD CONSTRAINT Strings_fk1 FOREIGN KEY ( Lang ) REFERENCES Languages ( Lang );
ALTER TABLE Strings ADD CONSTRAINT Strings_fk2 FOREIGN KEY ( Page_ID ) REFERENCES Page ( Page_ID );
ALTER TABLE VOICES ADD CONSTRAINT VOICES_fk1 FOREIGN KEY ( Lang ) REFERENCES Languages ( Lang );

