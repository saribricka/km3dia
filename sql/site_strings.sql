USE KM3LANG;
DELETE FROM Strings;
ALTER TABLE Strings AUTO_INCREMENT = 1;
DELETE FROM Page;
 
#PyrosomaKM3
#Logon_ok Intro
INSERT INTO Page VALUES(1);

INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',1,'Benvenuto',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',1,'il tuo livello utente è',2); 
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',1,'Scegli',3);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',1,'Seleziona una possibile azione e premi il tasto scegli.',4);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',1,': Funzione non trovata. Questa è una versione di prova del software.',5);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',1,'Selezionate una azione da compiere:',6);


INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',1,'Welcome',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',1,'your user level is',2);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',1,'Select',3);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',1,'Select a possible action and press the select button.',4);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',1,': Function not found. This is a test version of the software.',5);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',1,'Select a possible action.',6);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',1,'Scan or enter Bench ID.',7);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',1,'Select an Integration Site.',8);

#Modify User Level
INSERT INTO Page VALUES(2);

INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',2,'OID',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',2,'Nome',2);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',2,'Cognome',3);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',2,'Livello',4);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',2,'Cambia',5);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',2,"Trova l'operatore di cui vuoi settare il livello e premi il tasto ",6);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',2,' per modificarlo.',7);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',2,'Nuova chiave di accesso. Stamparla e premere  Cambia per salvarla nel Database.',8);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',2,'CERCA',9);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',2,'ERRORE: NON PUOI IMPOSTARE UN LIVELLO INFERIORE AL TUO.',10);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',2,'ERRORE: NON PUOI MODIFICARE IL LIVELLO DI UN UTENTE CON LIVELLO INFERIORE AL TUO.',11);

INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',2,'OID',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',2,'Name',2);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',2,'Surname',3);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',2,'Level',4);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',2,'Change',5);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',2,'Find the record you want to modify. Set level and press ',6);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',2,' to modify it.',7);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',2,'New user access key. Print it and press Change to store it in the Database.',8);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',2,'SEARCH',9);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',2,'ERROR: YOU ARE NOT PERMITTED TO SET LEVEL BELOW YOUR ONE.',10);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',2,'ERROR: YOU ARE NOT PERMITTED TO SET LEVEL OF A USER WITH A LEVEL BELOW YOUR ONE.',11);

#SetBench
INSERT INTO Page VALUES(3);

INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',3,'Descrizione',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',3,'Tipo',2);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',3,'Collocazione KM3',3);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',3,'Cambia',4);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',3,"Crea",5);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',3,' per modificarlo.',6);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',3,' Non ho trovato nessun bench alla tua locazione. Creane uno di tipo Magazzino.',7);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',3,"Magazzino",8);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',3,"Tavolo da Lavoro",9);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',3,"Elimina",10);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',3,"Crea un nuovo bench o modifica quelli esistenti. Puoi eliminarli solo se non sono stati utilizzati. ",11);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',3,"Errore: il Bench è già stato usato. ",12);



INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',3,'Description',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',3,'Type',2);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',3,'KM3 Location',3);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',3,'Change',4);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',3,'Create',5);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',3,' to modify it.',6);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',3,'No Bench at your location. Create one Storage type.',7);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',3,'Storage',8);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',3,'Work Bench',9);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',3,'Delete',10);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',3,"Create a new Bench or modify an existing one. You can delete a Bench only if it has not been used.",11);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',3,"Error: Bench already used . ",12);

#AddParts
INSERT INTO Page VALUES(4);

INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',4,'PBS',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',4,'VARIANT',2);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',4,'VERSION',3);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',4,'SERIAL',4);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',4,'POSIZIONE',5);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',4,'PARTE DI',6);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',4,'AGGIUNGI',7);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',4,'CERCA',8);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',4,'KM3db',9);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',4,'Elimina',10);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',4,'Elenco dei componenti presenti presso la vostra sede.',11);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',4,'Sceglere un bench prima di importare dal KM3_db.',12);
 

INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',4,'PBS',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',4,'VARIANT',2);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',4,'VERSION',3);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',4,'SERIAL',4);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',4,'POSITION',5);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',4,'PART OF',6);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',4,'ADD',7);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',4,'SEARCH',8);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',4,'KM3db Import',9);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',4,'Delete',10);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',4,'List of Parts present at your location.',11);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',4,'Choose a bench before KM3_db import.',12);
INSERT INTO Page VALUES(5);

INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',5,'Elenco degli UPI dei DOM preassegnati al sito.',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',5,'UPI',2);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',5,'INIZIA INTEGRAZIONE',3);


INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',5,'List of preassigned  DOM UPI.',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',5,'UPI',2);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',5,'START INTEGRATION',3);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',5,'Warning: Choosen Bench is a storage.',4);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',5,'Attenzione: Il Bench scelto è un deposito.',4);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',5,'Warning: No Bench selected.',5);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',5,'Attenzione: Nessun Bench è stato scelto.',5);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',5,'Warning: Bench occupied by integration.',6);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',5,'Attenzione: Il bench è già occupato da una integrazione',6);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',5,'RESUME INTEGRATION',7);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',5,'RIPRENDI INTEGRATION',7);

INSERT INTO Page VALUES(6); 
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',6,'Elenco degli UPI dei DOM preassegnati al sito.',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',6,'List of the DOM UPIs pre-assigned to the site.',1);

INSERT INTO Page VALUES(7);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',7,'Integrazione DOM',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',7,'DOM Integration',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',7,'Posizione nel DOM',2);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',7,'DOM Position',2);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',7,'UPI',3);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',7,'UPI',3);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',7,'ERRORE PBS: La parte selezionata non può essere integrata in questa posizione del DOM.',4);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',7,"PBS ERROR: The selected part can`t be integrated in that DOM position. ",4);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',7,'ERRORE: La parte selezionata è già stata integrata in un DOM.',5);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',7,"ERROR: The selected part has been already integrated in a DOM.",5);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',7,'ERRORE: Parte non presente nel Database. ',6);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',7,"ERROR: Part is not present in the Database. ",6);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',7,'ERRORE: La posizione è occupata. Liberarla prima di procedere. ',7);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',7,"ERROR: Position is occupied by another part. Empty position before integrating the new part.  ",7);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',7,'ERRORE: Non vi sono parti di qesto tipo. ',8);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',7,"ERROR: No parts of this type are present.  ",8);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',7,'ERRORE: Parte unica già presente nel database. ',9);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',7,"ERROR: Unique part already present in database.  ",9);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',7," È stata aggiunta al database per la posizione :  ",10);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',7," Has been added to database for position :  ",10);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',7," ERRORE: Il canale SFP non corrisponde a nessun D.O.M. assegnato.  ",11);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',7," ERROR: SFP channel do not match any assigned DOM. ",11);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',7," SFP integrata.  ",12);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',7," SFP has been integrated. ",12);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',7," È stata cambiato in :  ",13);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',7," Has been changed in : ",13);

INSERT INTO Page VALUES(8);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',8,'Integrazione',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',8,'Integration',1);

INSERT INTO Page VALUES(9);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',9,'Impostazioni',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',9,'Settings',1);

INSERT INTO Page VALUES(10);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',10,'Acquisizione Dati',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',10,'Data Acquisition',1);


#QR Strings
INSERT INTO Page VALUES(104);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',104,'Copia e incolla la figura del codice QR per usarla.',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',104,'Copy and past the QR image to use it.',1);
#Error Strings
INSERT INTO Page VALUES(999);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',999,'Bench occupato o DOM in integrazione in un altro Bench.',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',999,'Occupied Bench or DOM is being integrated in onother Bench.',1);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('en',999,'All componets on Bench and the DOM are moved to requested storage.',2);
INSERT INTO Strings (Lang,Page_ID,STRING,STR_order) VALUES('it',999,'Il DOM e tutti i componenti sul Bench sono stati spostati nel magazzino richesto.',2);
