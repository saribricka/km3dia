#INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('A_level','Description');
USE KM3DIA;
DELETE FROM PossibleOperations;

INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('11','Select Bench','1');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('9','Replace','0');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('9','Merge','0');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('9','To_Bench','0');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('9','To_Storage','0');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('9','HeTest','0');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('9','Splice_Test','0');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('9','Func_Test','0');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('5','Create_Bench','1');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('5','Modify_User_Level','1');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('5','AddPart','1');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('5','Search Assigned DOM','1');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('5','Assign Worker to Location','0');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('5','Integration','0');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('5','SetUp Integration','1');
INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('5','Resume Integration','1');

INSERT INTO PossibleOperations (A_level,Description,Visible)  VALUES('11','Logout','1');