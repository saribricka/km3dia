<?php
session_start();
define("DEBUG",true); // Turn on off debug prints 
define("MAX_BUFF", 500, true);
define("ULOAD_TMP",'/media/sf_CondivisoVB/tmp/');
define("ULOAD",'/media/sf_CondivisoVB/photo/');
define("NOLOGON",'1');
define("EXPTIME",'12'); // h validity of DAQ write key
define("ENCRYPTCOOKIE",1); //Enable Cookie encription.
define("DAQLEVEL",1); //DAQ USER LEVEL.
define("MONLEVEL",-2); //MONITOR USER LEVEL.
define("DEFPMTCFG",'default_pmt_cfg.xml'); //Default Configuration group for STANDARD PMT
define("DEFPMT_THRHVCFG",'default_pmt_thr_hv_cfg.xml'); //Default Configuration group for PMT with THR setting
define("RSDIR",'./RUNSETUPS/');

?>