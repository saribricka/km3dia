<?php
/**Functions are repeated in this INDEPENDENT service**/
  
  
	/*
		* 
		* Based on (forked from) the work by https://gist.github.com/Kostanos
		*
     
	*/
	ini_set("memory_limit","512M");
	set_time_limit ( 440 );
	function jsonToCsv ($json, $separator=",") {
		
		// See if the string contains something
		if (empty($json)) { 
			die("The JSON string is empty!");
		}
		
		// If passed a string, turn it into an array
		if (is_array($json) === false) {
			$json = json_decode($json, true);
		}
	
		
		$firstLineKeys = false;
		foreach ($json as $line) {
			if (empty($firstLineKeys)) {
				$firstLineKeys = array_keys($line);
				$SS=$SS.implode($separator,$firstLineKeys).chr(0x0D).chr(0x0A);
				$firstLineKeys = array_flip($firstLineKeys);
			}
				$SS=$SS.implode($separator,array_merge($firstLineKeys, $line)).chr(0x0D).chr(0x0A);
		}
		return $SS;
	}

function Userkey2TOK($UserCode)
{	/* 
	Decode User QR Code in Skey and KM3db_User_ID
	*/
	$UserCode_Tok = array();
	$tok = strtok($UserCode, ".");
	$UserCode_Tok['Skey']=$tok;
	$tok = strtok(".");
	$UserCode_Tok['KM3db_OID']=$tok;
	return $UserCode_Tok;
}
function OpenReadConn()
{
	//Opens a read olny connection with local DataBase Note that a special user can introduced
	$conn = new mysqli("localhost", apache_getenv("MYSQL_USER"), apache_getenv("MYSQL_R_PW"), "KM3DIA");
	$conn->set_charset ( 'utf-8' );
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	return $conn;
}
function charfilter($String)
{   /*
	Sanitize input
	*/
	$count=0;
	$forbidden= array("Operators");
	$String=str_replace($forbidden," ", $String,$count);
	
	// String lenght limited to MAX_BUFF
	return substr($String,0,MAX_BUFF);
}
$conn=OpenReadConn();
if ($_SERVER["REQUEST_METHOD"] == "GET"&&isset($_REQUEST["km3uname"])) 
		{   $usertok=Userkey2TOK( $_REQUEST['km3uname']);
			$Uoid=$usertok['KM3db_OID'];
			$Skey=$usertok['Skey'].".";
			$sql = "SELECT * FROM Operators where KM3db_OID = '".$Uoid."';";
			$result = $conn->query($sql);
			if ($result->num_rows == 1  ) {
				$Urow = $result->fetch_assoc();
				if (password_verify($Skey, $Urow['Skey'])||1) //DEBUG
				{
					if(isset($_REQUEST["REQ"]))
			{   $sql=$_REQUEST["REQ"];
				$result = $conn->query($_REQUEST["REQ"]);
				
				if (!($result)) {
					sqlog($sql,$conn->error);
				} else
					{   	$all=array();
						while($row = $result->fetch_assoc()) 
							{
								$all[]=$row;
							}
							$string=json_encode($all);
							if (isset($_REQUEST["CSV"]))
							{
								if ($_REQUEST["CSV"]==''){
							$string=jsonToCsv($string);}
							else {$string=jsonToCsv($string,$_REQUEST["CSV"]);}
							}
							if (isset($_REQUEST["BR"]))
							{
								echo nl2br($string);
							}
							else {
							echo $string;
							}
						}
					}
					
				}// END PWD VERIFY
			}
				
			
		}
/* 		$sql="SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='KM3DIA' 
    AND `TABLE_NAME`='Bench';";
	$result = $conn->query($sql);
	if (!($result)) {
					sqlog($sql,$conn->error);
				} else
					{   	
					$all=array();
						while($row = $result->fetch_assoc()) 
							{
								$all[]=$row;
							}
							$string=json_encode($all);
							
							echo nl2br($string);
					}
 */
$conn->close();
ini_set("memory_limit","30M");
	set_time_limit ( 30 );
?>