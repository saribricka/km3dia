<?php
require '/var/www/html/allinclude.php';
$Page=1; // This is Page 1 of KM3DIA
$ht_string=langreader($Page); // Page Strings 
$Urow=ureader (); // User DATA

if ($_SERVER["REQUEST_METHOD"] == "POST"&&isset($_REQUEST['km3action'])) 
{
    // collect value of input field
	
	$action_value = $_REQUEST['km3action'];
	$cookie_name = "Km3_action";
	setencryptedcookie($cookie_name, $action_value);
	if(!empty($_REQUEST['km3bench'])){ // php isset  true even for empty field
		$BenchC=BenchCode2TOK($_REQUEST['km3bench']);
		$bench_value = $BenchC['B_id'] ;
		$cookie_name = "km3bench";
		setencryptedcookie($cookie_name, $bench_value);
		$StartedDOM=OccupiedBench($bench_value);
	}
	
	if($_REQUEST['km3location']<>"")
	{ 
// php isset  true even for empty field
		if ($_REQUEST['km3location']<> $_COOKIE['Ilocation']) // il location is changed
		{		
				$cookie_name = "Ilocation";
				setencryptedcookie($cookie_name, $_REQUEST['km3location']);
				$cookie_name = "km3DIALA_IP"; // Reset AGENT COOKIE
				setcookie($cookie_name,'', time()-1000, '/');
				$cookie_name = "km3bench"; // Reset BENCH COOKIE
				setcookie($cookie_name,'', time()-1000, '/');
				$cookie_name = "O_Type";
				setcookie($cookie_name,'', time()-1000, '/');
				//Set Local var
				$con=OpenReadConn();
				$sql="select * from Site where SiteID='".$_REQUEST['km3location']."'";
				$cookie_name = "km3location";
				setencryptedcookie($cookie_name, $con->query($sql)->fetch_assoc()['KM3db_LOC_ID']);				
				$sql="select O_Type from ISites where SiteID='".$_REQUEST['km3location']."';";
				$cookie_name = "O_Type";
				setencryptedcookie($cookie_name, $con->query($sql)->fetch_assoc()['O_Type']);
				$sql="select SettingValue from Site_Settings where SiteID='".$_REQUEST['km3location']."' and SettingName='km3DIALA_IP' ;";
				$result = $con->query($sql);

				$con->close();
				if ($result->num_rows==1)
				{
				$IProw=$result->fetch_assoc();
				$cookie_name = "km3DIALA_IP";
				$cookie_value = $IProw['SettingValue'];
				setencryptedcookie($cookie_name, $cookie_value); // Set IP COOCKIE
				$key=uniqidReal(30); //KEY LENGHT =30   // Generate Temporary key
				$hashed=password_hash($key,PASSWORD_BCRYPT); //Compute Hashed key
				$Expire=time()+3600*EXPTIME;
				$cookie_name ="KMDIALA_TK";
				setencryptedcookie($cookie_name, $key); // Set key COOCKIE ----> Cookie will expire when browser is closed.
				$con=OpenWriteConn(); // STORE HASHED KEY
				$sql = "UPDATE Operators  set SSkey='".$hashed."',Expire='".$Expire."' where KM3db_OID ='".$Urow['KM3db_OID']."';";
				$result = $con->query($sql);
				if(!$result) sqlog($sql,$con->error);
				$con->close();

				}
				
				
		}
	//////// END SET LOCAL VAR 
	if (isset($_REQUEST['DELIVER']))
		gotopage('send2db.php');
	}
	
	
	switch ($action_value) {
	case "Select Bench":
	// If the selected bench is occupied start integration immediately
		if ($StartedDOM<>"") JumptoBench($StartedDOM);
		gotopage('logon_ok.php');
		break;
    case "Logout":
		gotopage('logout.php');
        
        break;
	case "Modify_User_Level":
		gotopage('modulevel.php');
		break;
	case "Create_Bench":
		gotopage('setbench.php');
		break;
	case "AddPart":
		gotopage('addpart.php');
		break;
	case "Search Assigned DOM":
		gotopage('assign_DOMl.php');
		break;
	case "SetUp Integration":
		gotopage('setup_integration.php');
		break;
	case "Site Settings":
		gotopage('site_settings.php');
	break;
	default:
		$cookie_name = "Km3_action";
		setencryptedcookie($cookie_name, "",time()-3600);
		ShowMessage($action_value.$ht_string[5],$ht_string[0]);
	}
	
	}
	
?>
<HTML>
<HEAD>
<style>
table {
  border-collapse: separate;
  border-spacing: 50px 0;
}

td {
  padding: 5px 0;
}
</style>


</HEAD>
<body>
<?php


	manubar();
	
	
	
 // Reads all Strings for Page. Strings are stored in $ht_string global array.
	$actions=areader($Urow["O_Level"]); // Finds possible Operations based on User Level.
	$Benchs=BenchReader();
	
	
	$string=$ht_string[1]."  ";
	$string=$string.$Urow["Name"]. "  " .$Urow["Surname"]." ";
	$string=$string.$ht_string[2].":"."  " .$Urow["O_Level"].". ";
    
    
	
	
    echo  "<BR>  ".$string."<BR>  " ;
	echo $ht_string[4];
	$string=$string." ".$ht_string[4];
	utter($string,$ht_string[0]);
	Hi_left();
	

	Low_left();
	echo '<div>'.$ht_string[6];
	echo '<FORM method="post">'; 
	echo '<select name="km3action" >';
	foreach($actions as $act){
		echo '<option value="'. $act.'">';
		echo $act."</option>";
	}
	echo "</select></DIV>";
	echo '<div>'.$ht_string[7];
	echo '<input type="text" name="km3bench" id="QRC"> ';
	echo '<button type="submit" ><i class="fas fa-check-circle" style="font-size:24px;color:green" ></i></button></DIV>';	
		
	if ($Urow["O_Level"]<0){
		$conn = OpenReadConn();
		$sql="SELECT * FROM ISites;";
		$result = $conn->query($sql);
		$conn->close();
		echo '<div>'.$ht_string[8];
		echo '<select name="km3location" >';
		
		if ($result->num_rows>0 ){
		while($row = $result->fetch_assoc()) { 
			
			echo '<option value="'.$row["SiteID"].'"';
			if ($_COOKIE["Ilocation"]==$row["SiteID"]) echo ' selected ';
			echo '>';
			echo $row["Town"]."</option>";
			}
		}
		
		echo '</select><button type="submit" title="DELIVER" name="DELIVER" title="Deliver DOMS to DB"><i class="fas fa-database" style="font-size:24px;color:red" ></i></button></DIV>';	
		
		}
	;
	
	echo '</FORM>';
	
	$result=WorkBenchReader();
	if ($result->num_rows>0 )
	{
		echo "<div><table class=' w3-striped w3-hoverable'>
    <thead >
		 <tr><td>Bench</td><td>DOM</td></tr>
    </thead>
    <tbody> ";// make ht string
	while($row = $result->fetch_assoc())
		{
		echo "<tr><td>".$row['B_Details']."</td><td>".$row['UPI']."</td>";
		echo '<td><button onClick="';
		echo "document.getElementById('QRC').value='".$row['B_id'].".".$_COOKIE["Ilocation"]."'";
		echo '"><i class="fa fa-share"></i></button></td></tr>';
		}
	echo '</tbody></table></div>';
	}

  footbar();

?>

</body>
</HTML>
